package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * The persistent class for the empresa_telefone database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="EMPRESA_TELEFONE")
@NamedQuery(name="EmpresaTelefone.findAll", query="SELECT e FROM EmpresaTelefone e")
public class EmpresaTelefone implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String numero;

	private String tipo; 

	@ManyToOne
	@JoinColumn(name = "id_empresa")
	private Empresa empresa;
}