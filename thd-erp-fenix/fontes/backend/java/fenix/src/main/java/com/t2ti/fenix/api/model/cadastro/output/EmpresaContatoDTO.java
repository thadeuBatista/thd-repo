package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmpresaContatoDTO {
	
	private int id;

	private String email;

	private String nome;

	private String observacao;

	private EmpresaDTO empresa;

}
