package com.t2ti.fenix.api.exceptionhandler;

import lombok.Getter;

@Getter
public enum ProblemType {
	
	ERRO_DE_SISTEMA("/erro-de-sistema", "Erro de sistema"),
	REQUISICAO_INVALIDA("/requisicao-invalida", "O corpo da requisição está com erros."),
	RECURSO_NAO_ENCONTRADO("/recurso-nao-encontrado", "Recurso não encontrado");
	
	private String title;
	private String uri;
	
	private ProblemType(String path, String title) {
		this.uri = "https://thd-erp.com.br" + path;
		this.title = title;
	}
}
