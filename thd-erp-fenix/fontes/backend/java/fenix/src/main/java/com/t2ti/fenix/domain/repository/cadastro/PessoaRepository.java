package com.t2ti.fenix.domain.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

	
}