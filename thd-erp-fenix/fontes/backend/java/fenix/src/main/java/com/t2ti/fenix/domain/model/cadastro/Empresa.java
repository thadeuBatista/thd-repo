package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the empresa database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="EMPRESA")
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String cnpj;

	private String contato;

	private String crt;

	@Temporal(TemporalType.DATE)
	@Column(name="data_constituicao")
	private Date dataConstituicao;

	private String email;

	@Column(name="inscricao_estadual")
	private String inscricaoEstadual;

	@Column(name="inscricao_municipal")
	private String inscricaoMunicipal;

	@Column(name="nome_fantasia")
	private String nomeFantasia;

	@Column(name="razao_social")
	private String razaoSocial;

	private String site;

	private String tipo;

	@Column(name="tipo_regime")
	private String tipoRegime;

	//bi-directional many-to-one association to EmpresaContato
	@OneToMany(mappedBy="empresa")
	private Set<EmpresaContato> empresaContatos;

	//bi-directional many-to-one association to EmpresaEndereco
	@OneToMany(mappedBy="empresa")
	private Set<EmpresaEndereco> empresaEnderecos;

	//bi-directional many-to-one association to EmpresaTelefone
	@OneToMany(mappedBy="empresa")
	private Set<EmpresaTelefone> empresaTelefones;

	//bi-directional many-to-one association to Setor
	@OneToMany(mappedBy="empresa")
	private Set<Setor> setors;
}