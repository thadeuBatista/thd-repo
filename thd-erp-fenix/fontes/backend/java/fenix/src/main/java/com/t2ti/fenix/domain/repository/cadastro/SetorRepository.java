package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Setor;

public interface SetorRepository extends JpaRepository<Setor, Integer> {

	List<Setor> findFirst10ByNomeContaining(String nome);
	
}