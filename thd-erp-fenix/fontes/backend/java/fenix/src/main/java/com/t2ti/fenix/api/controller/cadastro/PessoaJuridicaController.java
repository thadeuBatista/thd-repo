package com.t2ti.fenix.api.controller.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.fenix.domain.exception.RecursoNaoEncontradoException;
import com.t2ti.fenix.domain.model.cadastro.PessoaJuridica;
import com.t2ti.fenix.domain.service.cadastro.PessoaJuridicaService;

@RestController
@RequestMapping("/pessoaJuridicas")
public class PessoaJuridicaController {

	@Autowired
	private PessoaJuridicaService service;
	
	@GetMapping
	public List<PessoaJuridica> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public PessoaJuridica consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public PessoaJuridica salvar(@RequestBody PessoaJuridica pessoaJuridica) {
		return service.salvar(pessoaJuridica);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
