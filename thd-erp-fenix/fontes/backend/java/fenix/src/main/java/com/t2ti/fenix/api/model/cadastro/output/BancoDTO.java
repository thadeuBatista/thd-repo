package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BancoDTO {
	
	private int id;

	private String codigo;

	private String nome;

	private String url;

}
