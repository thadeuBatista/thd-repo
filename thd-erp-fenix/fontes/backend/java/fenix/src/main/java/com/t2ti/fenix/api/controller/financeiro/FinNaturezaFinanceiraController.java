/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Controller relacionado à tabela [FIN_NATUREZA_FINANCEIRA] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.api.controller.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.fenix.api.converter.financeiro.FinNaturezaFinanceiraConverter;
import com.t2ti.fenix.api.model.filter.Filtro;
import com.t2ti.fenix.api.model.financeiro.output.FinNaturezaFinanceiraDTO;
import com.t2ti.fenix.domain.model.financeiro.FinNaturezaFinanceira;
import com.t2ti.fenix.domain.service.financeiro.FinNaturezaFinanceiraService;


@RestController
@RequestMapping(value = "/fin-natureza-financeira", produces = "application/json;charset=UTF-8")
public class FinNaturezaFinanceiraController {

	@Autowired
	private FinNaturezaFinanceiraService service;

	@Autowired
	private FinNaturezaFinanceiraConverter converter;
	
	@GetMapping
	public List<FinNaturezaFinanceiraDTO> consultarLista(@RequestParam(required = false) String filter) {
		List<FinNaturezaFinanceira> listaObjeto;
		if (filter == null) {
			listaObjeto = this.service.consultarLista();
		} else {
			// define o filtro
			Filtro filtro = new Filtro(filter);
			listaObjeto = this.service.consultarLista(filtro);
		}
		
		List<FinNaturezaFinanceiraDTO> listaObjetoDTO = this.converter.toCollectionModel(listaObjeto);
		return listaObjetoDTO;
	}

	@GetMapping("/{id}")
	public FinNaturezaFinanceiraDTO consultarObjeto(@PathVariable Integer id) {
		FinNaturezaFinanceira objetoConsultado = this.service.consultarObjeto(id); 
		return this.converter.toModel(objetoConsultado);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public FinNaturezaFinanceiraDTO inserir(@RequestBody FinNaturezaFinanceiraDTO objJson) {
		FinNaturezaFinanceira objeto = this.converter.toDomainObject(objJson);
		objeto = this.service.salvar(objeto);
		return this.converter.toModel(objeto);
	}

	@PutMapping("/{id}")
	public FinNaturezaFinanceiraDTO alterar(@RequestBody FinNaturezaFinanceiraDTO objJson, @PathVariable Integer id) {
		FinNaturezaFinanceira objeto = this.service.consultarObjeto(id);
		this.converter.toDomainObject(objJson);
		objeto = service.salvar(objeto);
		
		return this.converter.toModel(objeto);	
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable Integer id) {
		this.service.excluir(id);
	}
	
}
