package com.t2ti.fenix.domain.repository.cadastro.filter;

public class BancoFilter {

	private String nome;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
