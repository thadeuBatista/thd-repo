package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the setor database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="SETOR")
@NamedQuery(name="Setor.findAll", query="SELECT s FROM Setor s")
public class Setor implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to Colaborador
	@OneToMany(mappedBy="setor")
	private Set<Colaborador> colaboradors;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="id_empresa")
	private Empresa empresa;


	public Colaborador addColaborador(Colaborador colaborador) {
		getColaboradors().add(colaborador);
		colaborador.setSetor(this);

		return colaborador;
	}

	public Colaborador removeColaborador(Colaborador colaborador) {
		getColaboradors().remove(colaborador);
		colaborador.setSetor(null);

		return colaborador;
	}

}