package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.Pessoa;
import com.t2ti.fenix.domain.repository.cadastro.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository repository;
	
	public List<Pessoa> listar() {
		return repository.findAll();
	}
	
	public List<Pessoa> listar(String nome) {
		return repository.findAll();
	}
	
	public Pessoa consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Pessoa salvar(Pessoa pessoa) {
		return repository.save(pessoa);
	}
	
	public void excluir(Integer id) {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		repository.delete(pessoa);
	}
	
}
