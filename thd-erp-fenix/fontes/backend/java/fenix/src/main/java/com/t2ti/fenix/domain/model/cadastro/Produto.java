package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the produto database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PRODUTO")
@NamedQuery(name="Produto.findAll", query="SELECT p FROM Produto p")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="codigo_interno")
	private String codigoInterno;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro;

	private String descricao;

	@Column(name="estoque_maximo")
	private BigDecimal estoqueMaximo;

	@Column(name="estoque_minimo")
	private BigDecimal estoqueMinimo;

	private String gtin;

	private String ncm;

	private String nome;

	@Column(name="quantidade_estoque")
	private BigDecimal quantidadeEstoque;

	@Column(name="valor_compra")
	private BigDecimal valorCompra;

	@Column(name="valor_venda")
	private BigDecimal valorVenda;

	//bi-directional many-to-one association to ProdutoMarca
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_produto_marca")
	private ProdutoMarca produtoMarca;

	//bi-directional many-to-one association to ProdutoSubgrupo
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_produto_subgrupo")
	private ProdutoSubgrupo produtoSubgrupo;

	//bi-directional many-to-one association to ProdutoUnidade
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_produto_unidade")
	private ProdutoUnidade produtoUnidade;


}