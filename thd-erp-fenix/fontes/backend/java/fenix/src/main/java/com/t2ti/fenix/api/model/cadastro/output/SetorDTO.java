package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SetorDTO {
	
	private int id;

	private String descricao;

	private String nome;

	private Set<ColaboradorDTO> colaboradors;

	private EmpresaDTO empresa;

}
