package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the produto_subgrupo database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PRODUTO_SUBGRUPO")
@NamedQuery(name="ProdutoSubgrupo.findAll", query="SELECT p FROM ProdutoSubgrupo p")
public class ProdutoSubgrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to Produto
	@OneToMany(mappedBy="produtoSubgrupo")
	private Set<Produto> produtos;

	//bi-directional many-to-one association to ProdutoGrupo
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_produto_grupo")
	private ProdutoGrupo produtoGrupo;


	public Produto addProduto(Produto produto) {
		getProdutos().add(produto);
		produto.setProdutoSubgrupo(this);

		return produto;
	}

	public Produto removeProduto(Produto produto) {
		getProdutos().remove(produto);
		produto.setProdutoSubgrupo(null);

		return produto;
	}
}