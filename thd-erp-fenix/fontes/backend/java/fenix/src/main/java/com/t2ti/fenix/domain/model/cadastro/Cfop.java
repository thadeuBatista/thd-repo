package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cfop database table.
 * 
 */
@Entity
@Table(name="CFOP")
@NamedQuery(name="Cfop.findAll", query="SELECT c FROM Cfop c")
public class Cfop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String aplicacao;

	private int codigo;

	private String descricao;

	public Cfop() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAplicacao() {
		return this.aplicacao;
	}

	public void setAplicacao(String aplicacao) {
		this.aplicacao = aplicacao;
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}