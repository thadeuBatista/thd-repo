/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Service relacionado à tabela [BANCO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.api.model.cadastro.output.BancoDTO;
import com.t2ti.fenix.api.model.filter.Filtro;
import com.t2ti.fenix.domain.exception.EntidadeEmUsoException;
import com.t2ti.fenix.domain.exception.EntidadeNaoEncontradaException;
import com.t2ti.fenix.domain.model.cadastro.Banco;
import com.t2ti.fenix.domain.repository.cadastro.BancoRepository;
import com.t2ti.fenix.domain.repository.cadastro.filter.BancoFilter;

@Service
public class BancoService {

	@Autowired
	private BancoRepository repository;
	
	@PersistenceContext
    private EntityManager entityManager;	
	
	public List<Banco> consultarLista() {
		return repository.findAll();
	}

	@SuppressWarnings("unchecked")
	public List<Banco> consultarLista(Filtro filtro) {
		String sql = "select * from BANCO where " + filtro.getWhere();
		Query query = entityManager.createNativeQuery(sql, Banco.class);
		return query.getResultList();
	}
	
	public Banco consultarObjeto(Integer id) {
		return repository.findById(id).
				orElseThrow(() -> new EntidadeNaoEncontradaException(
					String.format("Não existe um cadastro de Banco Ag com código %d", id)));
	}
	
	public Banco salvar(Banco objeto) {
		return repository.save(objeto);
	}
		
	public void excluir(Integer id) {
		try {
			repository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException(
					String.format("Não existe um cadastro de Banco Agencia com o codigo %d", id));
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
					String.format("Banco Ag de código %d não pode ser removido, pois está em uso", id));
		}
	}

	public List<Banco> pesquisar(BancoFilter filtro) {
		return this.repository.filtrar(filtro);
	}
	
}