package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the produto_marca database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PRODUTO_MARCA")
@NamedQuery(name="ProdutoMarca.findAll", query="SELECT p FROM ProdutoMarca p")
public class ProdutoMarca implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to Produto
	@OneToMany(mappedBy="produtoMarca")
	private Set<Produto> produtos;

	public Produto addProduto(Produto produto) {
		getProdutos().add(produto);
		produto.setProdutoMarca(this);

		return produto;
	}

	public Produto removeProduto(Produto produto) {
		getProdutos().remove(produto);
		produto.setProdutoMarca(null);

		return produto;
	}

}