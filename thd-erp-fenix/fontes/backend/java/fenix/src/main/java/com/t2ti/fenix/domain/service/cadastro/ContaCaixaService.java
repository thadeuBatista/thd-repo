package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.BancoContaCaixa;
import com.t2ti.fenix.domain.repository.cadastro.ContaCaixaRepository;

@Service
public class ContaCaixaService {

	@Autowired
	private ContaCaixaRepository repository;
	
	public List<BancoContaCaixa> listar() {
		return repository.findAll();
	}

	public List<BancoContaCaixa> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public BancoContaCaixa consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public BancoContaCaixa salvar(BancoContaCaixa bancoContaCaixa) {
		return repository.save(bancoContaCaixa);
	}
	
	public void excluir(Integer id) {
		BancoContaCaixa bancoContaCaixa = new BancoContaCaixa();
		bancoContaCaixa.setId(id);
		repository.delete(bancoContaCaixa);
	}
	
}
