package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.Setor;
import com.t2ti.fenix.domain.repository.cadastro.SetorRepository;

@Service
public class SetorService {

	@Autowired
	private SetorRepository repository;
	
	public List<Setor> listar() {
		return repository.findAll();
	}
	
	public List<Setor> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Setor consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Setor salvar(Setor setor) {
		return repository.save(setor);
	}
	
	public void excluir(Integer id) {
		Setor setor = new Setor();
		setor.setId(id);
		repository.delete(setor);
	}
	
}
