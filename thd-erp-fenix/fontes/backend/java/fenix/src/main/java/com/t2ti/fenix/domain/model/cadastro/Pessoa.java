package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the pessoa database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PESSOA")
@NamedQuery(name="Pessoa.findAll", query="SELECT p FROM Pessoa p")
public class Pessoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String cliente;

	private String colaborador;

	private String contador;

	private String email;

	private String fornecedor;

	private String nome;

	private String site;

	private String tipo;

	private String transportadora;
	
	/*
	@OneToOne(mappedBy="pessoa", cascade = CascadeType.ALL, orphanRemoval = true)
	private PessoaJuridica pessoaJuridica;

	@OneToMany(mappedBy="pessoa")
	private Set<Cliente> clientes;

	@OneToMany(mappedBy="pessoa")
	private Set<Colaborador> colaboradors;

	@OneToMany(mappedBy="pessoa")
	private Set<Contador> contadors;

	@OneToMany(mappedBy="pessoa")
	private Set<Fornecedor> fornecedors;

	@OneToMany(mappedBy="pessoa")
	private Set<PessoaContato> pessoaContatos;

	@OneToMany(mappedBy="pessoa")
	private Set<PessoaEndereco> pessoaEnderecos;

	@OneToMany(mappedBy="pessoa")
	private Set<PessoaFisica> pessoaFisicas;

	@OneToMany(mappedBy="pessoa")
	private Set<PessoaTelefone> pessoaTelefones;

	@OneToMany(mappedBy="pessoa")
	private Set<Transportadora> transportadoras;
	*/

}