package com.t2ti.fenix.domain.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Colaborador;

public interface ColaboradorRepository extends JpaRepository<Colaborador, Integer> {

}