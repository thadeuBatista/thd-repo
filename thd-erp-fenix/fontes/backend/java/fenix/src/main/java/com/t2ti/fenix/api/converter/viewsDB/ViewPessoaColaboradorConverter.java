/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Repository relacionado à tabela [VIEW_PESSOA_COLABORADOR] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.api.converter.viewsDB;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.t2ti.fenix.api.model.viewsDB.output.ViewPessoaColaboradorDTO;
import com.t2ti.fenix.domain.model.builder.viewsDB.ViewPessoaColaboradorBuilder;
import com.t2ti.fenix.domain.model.viewsDB.ViewPessoaColaborador;

@Component
public class ViewPessoaColaboradorConverter{


	/**
	 * Metodo que converte um objeto do tipo {@link ViewPessoaColaborador} em um objeto do tipo {@link ViewPessoaColaboradorDTO}
	 * @param ViewPessoaColaborador objeto que representa um ViewPessoaColaborador
	 * @return retorna um objeto do tipo {@link ViewPessoaColaboradorDTO}
	 */
	public ViewPessoaColaboradorDTO toModel(ViewPessoaColaborador objeto) {
		ViewPessoaColaboradorDTO dto = new ViewPessoaColaboradorDTO();
		BeanUtils.copyProperties(objeto, dto);
		//TODO		
		
		return dto;
	}

	public List<ViewPessoaColaboradorDTO> toCollectionModel(List<ViewPessoaColaborador> ViewPessoaColaboradors){
		return ViewPessoaColaboradors.stream()
				.map(ViewPessoaColaborador -> toModel(ViewPessoaColaborador))
				.collect(Collectors.toList());
	}

	/**
	 * Método que converte um objeto do tipo {@link ViewPessoaColaboradorDTO} em um objeto do tipo {@link ViewPessoaColaborador}
	 * @param ViewPessoaColaboradorInput Objeto que representa um {@link ViewPessoaColaboradorDTO}
	 * @return objeto do tipo {@link ViewPessoaColaborador}
	 */
	public ViewPessoaColaborador toDomainObject(ViewPessoaColaboradorDTO ViewPessoaColaboradorInput) {
		
		ViewPessoaColaborador objeto = ViewPessoaColaboradorBuilder.build();
		
		BeanUtils.copyProperties(ViewPessoaColaboradorInput, objeto);
		return objeto;
	}
	
}
