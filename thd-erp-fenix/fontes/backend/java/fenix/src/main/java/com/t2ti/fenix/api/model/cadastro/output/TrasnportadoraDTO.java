package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrasnportadoraDTO {

	private int id;

	private Date dataCadastro;

	private String observacao;

	private PessoaDTO pessoa;
}
