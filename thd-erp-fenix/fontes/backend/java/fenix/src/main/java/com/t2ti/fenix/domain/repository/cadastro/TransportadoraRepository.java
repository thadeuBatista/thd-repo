package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Transportadora;

public interface TransportadoraRepository extends JpaRepository<Transportadora, Integer> {

	List<Transportadora> findFirst10ByPessoaNomeContaining(String nome);
	
}