package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the produto_unidade database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PRODUTO_UNIDADE")
@NamedQuery(name="ProdutoUnidade.findAll", query="SELECT p FROM ProdutoUnidade p")
public class ProdutoUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String sigla;

	//bi-directional many-to-one association to Produto
	@OneToMany(mappedBy="produtoUnidade")
	private Set<Produto> produtos;

	public Produto addProduto(Produto produto) {
		getProdutos().add(produto);
		produto.setProdutoUnidade(this);

		return produto;
	}

	public Produto removeProduto(Produto produto) {
		getProdutos().remove(produto);
		produto.setProdutoUnidade(null);

		return produto;
	}

}