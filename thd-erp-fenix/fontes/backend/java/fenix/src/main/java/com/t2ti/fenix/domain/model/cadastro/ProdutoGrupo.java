package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the produto_grupo database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PRODUTO_GRUPO")
@NamedQuery(name="ProdutoGrupo.findAll", query="SELECT p FROM ProdutoGrupo p")
public class ProdutoGrupo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to ProdutoSubgrupo
	@OneToMany(mappedBy="produtoGrupo")
	private Set<ProdutoSubgrupo> produtoSubgrupos;

	public ProdutoSubgrupo addProdutoSubgrupo(ProdutoSubgrupo produtoSubgrupo) {
		getProdutoSubgrupos().add(produtoSubgrupo);
		produtoSubgrupo.setProdutoGrupo(this);

		return produtoSubgrupo;
	}

	public ProdutoSubgrupo removeProdutoSubgrupo(ProdutoSubgrupo produtoSubgrupo) {
		getProdutoSubgrupos().remove(produtoSubgrupo);
		produtoSubgrupo.setProdutoGrupo(null);

		return produtoSubgrupo;
	}

}