package com.t2ti.fenix.domain.model.builder.cadastro;

import com.t2ti.fenix.domain.model.cadastro.Produto;
import com.t2ti.fenix.domain.model.cadastro.ProdutoGrupo;
import com.t2ti.fenix.domain.model.cadastro.ProdutoMarca;
import com.t2ti.fenix.domain.model.cadastro.ProdutoSubgrupo;
import com.t2ti.fenix.domain.model.cadastro.ProdutoUnidade;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoBuilder {

	public static Produto build() {
		Produto objeto = new Produto();
		ProdutoSubgrupo objetoSubgrupo = new ProdutoSubgrupo();
		ProdutoGrupo objetoGrupo = new ProdutoGrupo();
		objetoSubgrupo.setProdutoGrupo(objetoGrupo);
		
		objeto.setProdutoMarca(new ProdutoMarca());
		objeto.setProdutoSubgrupo(objetoSubgrupo);
		objeto.setProdutoUnidade(new ProdutoUnidade());
		
		
		return objeto;
	}
}
