package com.t2ti.fenix.domain.repository.cadastro.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.t2ti.fenix.domain.model.cadastro.Banco;
import com.t2ti.fenix.domain.repository.cadastro.filter.BancoFilter;
import com.t2ti.fenix.domain.repository.cadastro.query.BancoRepositoryQuery;

public class BancoRepositoryImpl implements BancoRepositoryQuery {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Banco> filtrar(BancoFilter filtro) {
		String sql = "select * from BANCO where " + montarWhere(filtro);
		Query query = this.manager.createNativeQuery(sql, Banco.class);
		return query.getResultList();
	}
	
	private String montarWhere(BancoFilter filtro) {
		String where = " 1 = 1 ";
		if(filtro.getNome() != null) {
			where += " and LOWER(nome) like '%" + filtro.getNome().toLowerCase() + "%' ";
		}
		return where;
	}

	
}
