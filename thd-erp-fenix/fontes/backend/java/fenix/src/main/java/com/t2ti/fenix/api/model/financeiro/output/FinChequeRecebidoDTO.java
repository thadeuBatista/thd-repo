/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Model relacionado à tabela [FIN_CHEQUE_RECEBIDO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.api.model.financeiro.output;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.t2ti.fenix.api.model.cadastro.output.PessoaDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FinChequeRecebidoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    private Integer id;

    private String cpf;

    private String cnpj;

    private String nome;

    private String codigoBanco;

    private String codigoAgencia;

    private String conta;

    private Integer numero;

    private Date dataEmissao;

    private Date bomPara;

    private Date dataCompensacao;

    private BigDecimal valor;

    private Date custodiaData;

    private BigDecimal custodiaTarifa;

    private BigDecimal custodiaComissao;

    private Date descontoData;

    private BigDecimal descontoTarifa;

    private BigDecimal descontoComissao;

    private BigDecimal valorRecebido;

    private PessoaDTO pessoa;

	public FinChequeRecebidoDTO() {
		//construtor padrão
	}

		
}
