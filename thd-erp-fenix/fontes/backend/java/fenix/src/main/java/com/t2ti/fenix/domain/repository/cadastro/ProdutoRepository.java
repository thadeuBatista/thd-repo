package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Produto;
import com.t2ti.fenix.domain.repository.cadastro.query.ProdutoRepositoryQuery;

public interface ProdutoRepository extends JpaRepository<Produto, Integer>, ProdutoRepositoryQuery {

	List<Produto> findFirst10ByNomeContaining(String nome);
	
}