package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.BancoContaCaixa;

public interface ContaCaixaRepository extends JpaRepository<BancoContaCaixa, Integer> {

	List<BancoContaCaixa> findFirst10ByNomeContaining(String nome);
	
}