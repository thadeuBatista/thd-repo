package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FornecedorDTO {

	private int id;

	private Date dataCadastro;

	private Date desde;

	private String observacao;

	private PessoaDTO pessoa;
}
