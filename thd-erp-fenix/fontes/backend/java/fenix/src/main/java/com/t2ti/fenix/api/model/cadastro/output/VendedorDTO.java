package com.t2ti.fenix.api.model.cadastro.output;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VendedorDTO {
	
	private int id;

	private BigDecimal comissao;

	private BigDecimal metaVenda;

	private ColaboradorDTO colaborador;

}
