package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the funcao database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="FUNCAO")
@NamedQuery(name="Funcao.findAll", query="SELECT f FROM Funcao f")
public class Funcao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to PapelFuncao
	@OneToMany(mappedBy="funcao")
	private Set<PapelFuncao> papelFuncaos;

	public PapelFuncao addPapelFuncao(PapelFuncao papelFuncao) {
		getPapelFuncaos().add(papelFuncao);
		papelFuncao.setFuncao(this);

		return papelFuncao;
	}

	public PapelFuncao removePapelFuncao(PapelFuncao papelFuncao) {
		getPapelFuncaos().remove(papelFuncao);
		papelFuncao.setFuncao(null);

		return papelFuncao;
	}

}