package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;


/**
 * The persistent class for the papel database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PAPEL")
@NamedQuery(name="Papel.findAll", query="SELECT p FROM Papel p")
public class Papel implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String nome;

	//bi-directional many-to-one association to PapelFuncao
	@OneToMany(mappedBy="papel")
	private Set<PapelFuncao> papelFuncaos;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="papel")
	private Set<Usuario> usuarios;

	public PapelFuncao addPapelFuncao(PapelFuncao papelFuncao) {
		getPapelFuncaos().add(papelFuncao);
		papelFuncao.setPapel(this);

		return papelFuncao;
	}

	public PapelFuncao removePapelFuncao(PapelFuncao papelFuncao) {
		getPapelFuncaos().remove(papelFuncao);
		papelFuncao.setPapel(null);

		return papelFuncao;
	}

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setPapel(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setPapel(null);

		return usuario;
	}

}