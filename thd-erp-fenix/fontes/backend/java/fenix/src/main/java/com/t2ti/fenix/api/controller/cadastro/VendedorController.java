package com.t2ti.fenix.api.controller.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.fenix.domain.exception.RecursoNaoEncontradoException;
import com.t2ti.fenix.domain.model.cadastro.Vendedor;
import com.t2ti.fenix.domain.service.cadastro.VendedorService;

@RestController
@RequestMapping("/vendedor")
public class VendedorController {

	@Autowired
	private VendedorService service;
	
	@GetMapping
	public List<Vendedor> listar() {
		return service.listar();
	}

	@GetMapping("/lista/{nome}")
	public List<Vendedor> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public Vendedor consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public Vendedor salvar(@RequestBody Vendedor vendedor) {
		return service.salvar(vendedor);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
