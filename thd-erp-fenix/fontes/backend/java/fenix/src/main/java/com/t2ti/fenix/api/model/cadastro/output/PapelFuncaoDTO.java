package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PapelFuncaoDTO {

	private int id;

	private String habilitado;

	private String podeAlterar;

	private String podeExcluir;

	private String podeInserir;

	private FuncaoDTO funcao;

	private PapelDTO papel;
}
