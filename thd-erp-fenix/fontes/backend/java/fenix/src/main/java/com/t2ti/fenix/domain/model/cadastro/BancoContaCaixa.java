package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * The persistent class for the banco_conta_caixa database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="BANCO_CONTA_AGENCIA")
@NamedQuery(name="BancoContaCaixa.findAll", query="SELECT b FROM BancoContaCaixa b")
public class BancoContaCaixa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descricao;

	private String digito;

	private String nome;

	private String numero;

	private String tipo;

	//bi-directional many-to-one association to BancoAgencia
	@ManyToOne
	@JoinColumn(name="id_banco_agencia")
	private BancoAgencia bancoAgencia;


}