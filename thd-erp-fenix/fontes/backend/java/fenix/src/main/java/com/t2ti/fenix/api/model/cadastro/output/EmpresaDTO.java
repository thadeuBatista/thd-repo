package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Date;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmpresaDTO {
	
	private int id;

	private String cnpj;

	private String contato;

	private String crt;

	private Date dataConstituicao;

	private String email;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private String nomeFantasia;

	private String razaoSocial;

	private String site;

	private String tipo;

	private String tipoRegime;

	private Set<EmpresaContatoDTO> empresaContatos;

	private Set<EmpresaEnderecoDTO> empresaEnderecos;

	private Set<EmpresaTelefoneDTO> empresaTelefones;

	private Set<SetorDTO> setors;

}
