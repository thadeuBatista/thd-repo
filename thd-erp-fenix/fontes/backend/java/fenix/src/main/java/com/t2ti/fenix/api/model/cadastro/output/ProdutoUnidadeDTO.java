package com.t2ti.fenix.api.model.cadastro.output;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoUnidadeDTO {
	
	public ProdutoUnidadeDTO() {
		this.produtos = new HashSet<ProdutoDTO>();
	}

	private int id;

	private String descricao;

	private String sigla;

	private Set<ProdutoDTO> produtos;
}
