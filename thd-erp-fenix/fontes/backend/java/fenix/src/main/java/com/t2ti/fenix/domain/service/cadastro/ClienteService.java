package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.Cliente;
import com.t2ti.fenix.domain.repository.cadastro.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	public List<Cliente> listar() {
		return repository.findAll();
	}

	public List<Cliente> listar(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}
	
	public Cliente consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Cliente salvar(Cliente cliente) {
		return repository.save(cliente);
	}
	
	public void excluir(Integer id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		repository.delete(cliente);
	}
	
}
