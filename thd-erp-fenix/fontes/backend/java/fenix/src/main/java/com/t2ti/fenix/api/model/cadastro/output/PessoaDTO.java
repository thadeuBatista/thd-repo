package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PessoaDTO {

	private int id;

	private String cliente;

	private String colaborador;

	private String contador;

	private String email;

	private String fornecedor;

	private String nome;

	private String site;

	private String tipo;

	private String transportadora;
	
	//bi-directional many-to-one association to PessoaJuridica
	/*@OneToOne(mappedBy="pessoa", cascade = CascadeType.ALL, orphanRemoval = true)
	private PessoaJuridica pessoaJuridica;*/

	/*//bi-directional many-to-one association to Cliente
	@OneToMany(mappedBy="pessoa")
	private Set<Cliente> clientes;

	//bi-directional many-to-one association to Colaborador
	@OneToMany(mappedBy="pessoa")
	private Set<Colaborador> colaboradors;

	//bi-directional many-to-one association to Contador
	@OneToMany(mappedBy="pessoa")
	private Set<Contador> contadors;

	//bi-directional many-to-one association to Fornecedor
	@OneToMany(mappedBy="pessoa")
	private Set<Fornecedor> fornecedors;

	//bi-directional many-to-one association to PessoaContato
	@OneToMany(mappedBy="pessoa")
	private Set<PessoaContato> pessoaContatos;

	//bi-directional many-to-one association to PessoaEndereco
	@OneToMany(mappedBy="pessoa")
	private Set<PessoaEndereco> pessoaEnderecos;

	//bi-directional many-to-one association to PessoaFisica
	@OneToMany(mappedBy="pessoa")
	private Set<PessoaFisica> pessoaFisicas;

	

	//bi-directional many-to-one association to PessoaTelefone
	@OneToMany(mappedBy="pessoa")
	private Set<PessoaTelefone> pessoaTelefones;

	//bi-directional many-to-one association to Transportadora
	@OneToMany(mappedBy="pessoa")
	private Set<Transportadora> transportadoras;*/
}
