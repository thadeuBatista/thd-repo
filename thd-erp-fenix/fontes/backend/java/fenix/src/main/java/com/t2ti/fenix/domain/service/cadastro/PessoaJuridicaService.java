package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.PessoaJuridica;
import com.t2ti.fenix.domain.repository.cadastro.PessoaJuridicaRepository;

@Service
public class PessoaJuridicaService {

	@Autowired
	private PessoaJuridicaRepository repository;
	
	public List<PessoaJuridica> listar() {
		return repository.findAll();
	}
	
	public PessoaJuridica consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public PessoaJuridica salvar(PessoaJuridica pessoaJuridica) {
		return repository.save(pessoaJuridica);
	}
	
	public void excluir(Integer id) {
		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		pessoaJuridica.setId(id);
		repository.delete(pessoaJuridica);
	}
	
}
