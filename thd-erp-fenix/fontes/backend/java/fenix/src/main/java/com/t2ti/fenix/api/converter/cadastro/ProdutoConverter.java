package com.t2ti.fenix.api.converter.cadastro;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.t2ti.fenix.api.model.cadastro.output.ProdutoDTO;
import com.t2ti.fenix.domain.model.builder.cadastro.ProdutoBuilder;
import com.t2ti.fenix.domain.model.cadastro.Produto;
import com.t2ti.fenix.domain.model.cadastro.ProdutoMarca;
import com.t2ti.fenix.domain.model.cadastro.ProdutoSubgrupo;
import com.t2ti.fenix.domain.model.cadastro.ProdutoUnidade;

@Component
public class ProdutoConverter {



	/**
	 * Metodo que converte um objeto do tipo {@link Produto} em um objeto do tipo {@link ProdutoDTO}
	 * @param produto objeto que representa um produto
	 * @return retorna um objeto do tipo {@link ProdutoDTO}
	 */
	public ProdutoDTO toModel(Produto objeto) {
		ProdutoDTO dto = new ProdutoDTO();
		BeanUtils.copyProperties(objeto, dto);
		BeanUtils.copyProperties(objeto.getProdutoMarca(), dto.getProdutoMarca(), "produtos");
		BeanUtils.copyProperties(objeto.getProdutoSubgrupo(), dto.getProdutoSubgrupo(), "produtos");
		BeanUtils.copyProperties(objeto.getProdutoSubgrupo().getProdutoGrupo(), dto.getProdutoSubgrupo().getProdutoGrupo(), "produtoSubgrupos");
		BeanUtils.copyProperties(objeto.getProdutoUnidade(), dto.getProdutoUnidade(), "produtos");
		return dto;
		
		//return this.modelMapper.map(objeto,  ProdutoDTO.class);
	}
	
	public List<ProdutoDTO> toCollectionModel(List<Produto> produtos){
		return produtos.stream()
				.map(produto -> toModel(produto))
				.collect(Collectors.toList());
	}
	
	/**
	 * Método que converte um objeto do tipo {@link ProdutoDTO} em um objeto do tipo {@link Produto}
	 * @param produtoInput Objeto que representa um {@link ProdutoDTO}
	 * @return objeto do tipo {@link Produto}
	 */
	public Produto toDomainObject(ProdutoDTO produtoInput) {
		
		Produto objeto = ProdutoBuilder.build();
		
		BeanUtils.copyProperties(produtoInput, objeto);
		BeanUtils.copyProperties(produtoInput.getProdutoMarca(), objeto.getProdutoMarca());
		BeanUtils.copyProperties(produtoInput.getProdutoSubgrupo(), objeto.getProdutoSubgrupo());
		BeanUtils.copyProperties(produtoInput.getProdutoSubgrupo().getProdutoGrupo(), objeto.getProdutoSubgrupo().getProdutoGrupo());
		BeanUtils.copyProperties(produtoInput.getProdutoUnidade(), objeto.getProdutoUnidade());
		return objeto;
	}
	
	
}
