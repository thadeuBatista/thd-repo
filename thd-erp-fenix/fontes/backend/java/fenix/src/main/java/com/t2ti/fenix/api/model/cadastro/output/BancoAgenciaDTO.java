package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BancoAgenciaDTO {
	
	private int id;

	private String contato;

	private String digito;

	private String nome;

	private String numero;

	private String observacao;

	private String telefone;

	private BancoDTO banco;

	private Set<BancoContaCaixaDTO> bancoContaCaixas;

}
