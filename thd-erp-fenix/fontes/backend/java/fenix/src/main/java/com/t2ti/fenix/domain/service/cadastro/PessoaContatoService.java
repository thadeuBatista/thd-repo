package com.t2ti.fenix.domain.service.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.fenix.domain.model.cadastro.PessoaContato;
import com.t2ti.fenix.domain.repository.cadastro.PessoaContatoRepository;

@Service
public class PessoaContatoService {

	@Autowired
	private PessoaContatoRepository repository;
	
	public List<PessoaContato> listar() {
		return repository.findAll();
	}
	
	public PessoaContato consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public PessoaContato salvar(PessoaContato pessoaContato) {
		return repository.save(pessoaContato);
	}
	
	public void excluir(Integer id) {
		PessoaContato pessoaContato = new PessoaContato();
		pessoaContato.setId(id);
		repository.delete(pessoaContato);
	}
	
}
