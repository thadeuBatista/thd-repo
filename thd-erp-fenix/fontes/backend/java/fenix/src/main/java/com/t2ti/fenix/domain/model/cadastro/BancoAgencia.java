package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * The persistent class for the banco_agencia database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="BANCO_AGENCIA")
@NamedQuery(name="BancoAgencia.findAll", query="SELECT b FROM BancoAgencia b")
public class BancoAgencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String contato;

	private String digito;

	private String nome;

	private String numero;

	private String observacao;

	private String telefone;

	//bi-directional many-to-one association to Banco
	@ManyToOne
	@JoinColumn(name="id_banco")
	private Banco banco;
/*
	//bi-directional many-to-one association to BancoContaCaixa
	@OneToMany(mappedBy="bancoAgencia")
	private Set<BancoContaCaixa> bancoContaCaixas;


	public BancoContaCaixa addBancoContaCaixa(BancoContaCaixa bancoContaCaixa) {
		getBancoContaCaixas().add(bancoContaCaixa);
		bancoContaCaixa.setBancoAgencia(this);

		return bancoContaCaixa;
	}

	public BancoContaCaixa removeBancoContaCaixa(BancoContaCaixa bancoContaCaixa) {
		getBancoContaCaixas().remove(bancoContaCaixa);
		bancoContaCaixa.setBancoAgencia(null);

		return bancoContaCaixa;
	}
*/
}