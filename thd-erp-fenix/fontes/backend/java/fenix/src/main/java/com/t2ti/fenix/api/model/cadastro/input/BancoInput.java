package com.t2ti.fenix.api.model.cadastro.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BancoInput {
	
	private String codigo;

	private String nome;

	private String url;

}
