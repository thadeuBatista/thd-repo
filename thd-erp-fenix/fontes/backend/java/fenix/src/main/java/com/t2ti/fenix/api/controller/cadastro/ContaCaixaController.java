package com.t2ti.fenix.api.controller.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.fenix.domain.exception.RecursoNaoEncontradoException;
import com.t2ti.fenix.domain.model.cadastro.BancoContaCaixa;
import com.t2ti.fenix.domain.service.cadastro.ContaCaixaService;

@RestController
@RequestMapping("/conta-caixa")
public class ContaCaixaController {

	@Autowired
	private ContaCaixaService service;
	
	@GetMapping
	public List<BancoContaCaixa> listar() {
		return service.listar();
	}

	@GetMapping("/lista/{nome}")
	public List<BancoContaCaixa> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public BancoContaCaixa consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public BancoContaCaixa salvar(@RequestBody BancoContaCaixa bancoContaCaixa) {
		return service.salvar(bancoContaCaixa);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
