/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Repository relacionado à tabela [FIN_CHEQUE_EMITIDO] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.api.converter.financeiro;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.t2ti.fenix.api.model.financeiro.output.FinChequeEmitidoDTO;
import com.t2ti.fenix.domain.model.builder.financeiro.FinChequeEmitidoBuilder;
import com.t2ti.fenix.domain.model.financeiro.FinChequeEmitido;

@Component
public class FinChequeEmitidoConverter{


	/**
	 * Metodo que converte um objeto do tipo {@link FinChequeEmitido} em um objeto do tipo {@link FinChequeEmitidoDTO}
	 * @param FinChequeEmitido objeto que representa um FinChequeEmitido
	 * @return retorna um objeto do tipo {@link FinChequeEmitidoDTO}
	 */
	public FinChequeEmitidoDTO toModel(FinChequeEmitido objeto) {
		FinChequeEmitidoDTO dto = new FinChequeEmitidoDTO();
		BeanUtils.copyProperties(objeto, dto);
		//TODO		
		
		return dto;
	}

	public List<FinChequeEmitidoDTO> toCollectionModel(List<FinChequeEmitido> FinChequeEmitidos){
		return FinChequeEmitidos.stream()
				.map(FinChequeEmitido -> toModel(FinChequeEmitido))
				.collect(Collectors.toList());
	}

	/**
	 * Método que converte um objeto do tipo {@link FinChequeEmitidoDTO} em um objeto do tipo {@link FinChequeEmitido}
	 * @param FinChequeEmitidoInput Objeto que representa um {@link FinChequeEmitidoDTO}
	 * @return objeto do tipo {@link FinChequeEmitido}
	 */
	public FinChequeEmitido toDomainObject(FinChequeEmitidoDTO FinChequeEmitidoInput) {
		
		FinChequeEmitido objeto = FinChequeEmitidoBuilder.build();
		
		BeanUtils.copyProperties(FinChequeEmitidoInput, objeto);
		return objeto;
	}
	
}
