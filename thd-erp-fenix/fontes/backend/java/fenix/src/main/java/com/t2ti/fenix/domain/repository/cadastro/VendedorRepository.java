package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Vendedor;

public interface VendedorRepository extends JpaRepository<Vendedor, Integer> {

	List<Vendedor> findFirst10ByColaboradorPessoaNomeContaining(String nome);
	
}