package com.t2ti.fenix.api.model.cadastro.output;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoDTO {
	
	public ProdutoDTO() {
		this.produtoMarca = new ProdutoMarcaDTO();
		this.produtoSubgrupo = new ProdutoSubgrupoDTO();
		this.produtoUnidade = new ProdutoUnidadeDTO();
	}

	private int id;

	private String codigoInterno;

	private Date dataCadastro;

	private String descricao;

	private BigDecimal estoqueMaximo;

	private BigDecimal estoqueMinimo;

	private String gtin;

	private String ncm;

	private String nome;

	private BigDecimal quantidadeEstoque;

	private BigDecimal valorCompra;

	private BigDecimal valorVenda;

	private ProdutoMarcaDTO produtoMarca;

	private ProdutoSubgrupoDTO produtoSubgrupo;

	private ProdutoUnidadeDTO produtoUnidade;
}
