package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * The persistent class for the pessoa_juridica database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PESSOA_JURIDICA")
@NamedQuery(name="PessoaJuridica.findAll", query="SELECT p FROM PessoaJuridica p")
public class PessoaJuridica implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String cnpj;

	private String crt;

	@Temporal(TemporalType.DATE)
	@Column(name="data_constituicao")
	private Date dataConstituicao;

	@Column(name="inscricao_estadual")
	private String inscricaoEstadual;

	@Column(name="inscricao_municipal")
	private String inscricaoMunicipal;

	@Column(name="nome_fantasia")
	private String nomeFantasia;

	@Column(name="tipo_regime")
	private String tipoRegime;

	//bi-directional many-to-one association to Pessoa
	@OneToOne
	@JoinColumn(name="id_pessoa")
	private Pessoa pessoa;


}