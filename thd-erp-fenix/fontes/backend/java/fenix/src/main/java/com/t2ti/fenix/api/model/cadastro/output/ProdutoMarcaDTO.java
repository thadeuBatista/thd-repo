package com.t2ti.fenix.api.model.cadastro.output;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoMarcaDTO {
	
	public ProdutoMarcaDTO() {
		this.produtos = new HashSet<ProdutoDTO>();
	}

	private int id;

	private String descricao;

	private String nome;

	private Set<ProdutoDTO> produtos;
}
