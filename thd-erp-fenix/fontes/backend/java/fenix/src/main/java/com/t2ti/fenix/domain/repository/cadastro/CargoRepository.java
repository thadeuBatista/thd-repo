package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Cargo;

public interface CargoRepository extends JpaRepository<Cargo, Integer> {

	List<Cargo> findFirst10ByNomeContaining(String nome);
	
}