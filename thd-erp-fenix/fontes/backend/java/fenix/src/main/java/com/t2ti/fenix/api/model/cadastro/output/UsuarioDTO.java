package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDTO {

	private int id;

	private String administrador;

	private Date dataCadastro;

	private String login;

	private String senha;

	private ColaboradorDTO colaborador;

	private PapelDTO papel;
}
