package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoGrupoDTO {

	private int id;

	private String descricao;

	private String nome;

	private Set<ProdutoSubgrupoDTO> produtoSubgrupos;
}
