package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmpresaTelefoneDTO {

	private int id;

	private String numero;

	private String tipo;

	private EmpresaDTO empresa;
}
