package com.t2ti.fenix.api.model.cadastro.output;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoSubgrupoDTO {
	public ProdutoSubgrupoDTO() {
		this.produtoGrupo = new ProdutoGrupoDTO();
		this.produtos = new HashSet<ProdutoDTO>();
	}

	private int id;

	private String descricao;

	private String nome;

	private Set<ProdutoDTO> produtos;

	private ProdutoGrupoDTO produtoGrupo;
}
