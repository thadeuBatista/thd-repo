package com.t2ti.fenix.domain.repository.cadastro.query;

import java.util.List;

import com.t2ti.fenix.domain.model.cadastro.Banco;
import com.t2ti.fenix.domain.repository.cadastro.filter.BancoFilter;

public interface BancoRepositoryQuery {

	public List<Banco> filtrar(BancoFilter filtro);
}
