package com.t2ti.fenix.api.controller.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.t2ti.fenix.domain.exception.RecursoNaoEncontradoException;
import com.t2ti.fenix.domain.exception.UploadException;
import com.t2ti.fenix.domain.model.cadastro.Colaborador;
import com.t2ti.fenix.domain.service.cadastro.ColaboradorService;

@RestController
@RequestMapping("/colaborador")
public class ColaboradorController {

	@Autowired
	private ColaboradorService service;
	
	@GetMapping
	public List<Colaborador> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public Colaborador consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public Colaborador salvar(@RequestBody Colaborador colaborador) {
		return service.salvar(colaborador);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}

	@PostMapping("/upload/{id}")
	public void uploadFoto(@RequestParam("foto") MultipartFile file, @PathVariable Integer id) {
		try {
			service.uploadFoto(file, id);
		} catch (Exception e) {
			throw new UploadException(e.getMessage());
		}
	}
}
