package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BancoContaCaixaDTO {

	private int id;

	private String descricao;

	private String digito;

	private String nome;

	private String numero;

	private String tipo;

	private BancoAgenciaDTO bancoAgencia;
}
