package com.t2ti.fenix.api.model.cadastro.output;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmpresaEnderecoDTO {
	
	private int id;

	private String bairro;

	private String cep;

	private String cidade;

	private String logradouro;

	private int municipioIbge;

	private String numero;

	private String uf;

	private EmpresaDTO empresa;

}
