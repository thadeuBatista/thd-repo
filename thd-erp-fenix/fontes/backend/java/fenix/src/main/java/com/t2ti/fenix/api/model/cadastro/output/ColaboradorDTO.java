package com.t2ti.fenix.api.model.cadastro.output;

import java.util.Date;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ColaboradorDTO {
	
	private int id;

	private Date ctpsDataExpedicao;

	private String ctpsNumero;

	private String ctpsSerie;

	private String ctpsUf;

	private Date dataAdmissao;

	private Date dataCadastro;

	private Date dataDemissao;

	private String matricula;

	private String observacao;

	private CargoDTO cargo;

	private PessoaDTO pessoa;

	private SetorDTO setor;

	private Set<UsuarioDTO> usuarios;

	private Set<VendedorDTO> vendedors;

}
