package com.t2ti.fenix.domain.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.PessoaContato;

public interface PessoaContatoRepository extends JpaRepository<PessoaContato, Integer> {

	
}