package com.t2ti.fenix.api.converter.cadastro;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.t2ti.fenix.api.model.cadastro.input.BancoInput;
import com.t2ti.fenix.api.model.cadastro.output.BancoDTO;
import com.t2ti.fenix.domain.model.cadastro.Banco;

@Component
public class BancoConverter {
	

	/**
	 * Metodo que converte um objeto do tipo {@link Banco} em um objeto do tipo {@link BancoDTO}
	 * @param banco objeto que representa um banco
	 * @return retorna um objeto do tipo {@link BancoDTO}
	 */
	public BancoDTO toModel(Banco banco) {
		return null;//this.modelMapper.map(banco,  BancoDTO.class);
	}
	
	public List<BancoDTO> toCollectionModel(List<Banco> bancos){
		
		return bancos.stream()
				.map(banco -> toModel(banco))
				.collect(Collectors.toList());
	}
	
	/**
	 * Método que converte um objeto do tipo {@link BancoInput} em um objeto do tipo {@link Banco}
	 * @param bancoInput Objeto que representa um {@link BancoInput}
	 * @return objeto do tipo {@link Banco}
	 */
	public Banco toDomainObject(BancoInput bancoInput) {
		return null; //this.modelMapper.map(bancoInput,  Banco.class);
	}
	
	/**
	 * Método para evitar a org.hibernate.HibernateException: identifier of an instance of 
	 * Banco was altered from 1 to 2
	 * @param {@link BancoInput}, {@link Banco}
	 * @param {@link Banco}
	 */
	public void copyToDomainObject(BancoInput bancoInput, Banco banco) {
		//modelMapper.map(bancoInput, banco);
	}
}
