package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the colaborador database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="COLABORADOR")
@NamedQuery(name="Colaborador.findAll", query="SELECT c FROM Colaborador c")
public class Colaborador implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="ctps_data_expedicao")
	private Date ctpsDataExpedicao;

	@Column(name="ctps_numero")
	private String ctpsNumero;

	@Column(name="ctps_serie")
	private String ctpsSerie;

	@Column(name="ctps_uf")
	private String ctpsUf;

	@Temporal(TemporalType.DATE)
	@Column(name="data_admissao")
	private Date dataAdmissao;

	@Temporal(TemporalType.DATE)
	@Column(name="data_cadastro")
	private Date dataCadastro;

	@Temporal(TemporalType.DATE)
	@Column(name="data_demissao")
	private Date dataDemissao;

	private String matricula;

	private String observacao;

	//bi-directional many-to-one association to Cargo
	@ManyToOne
	@JoinColumn(name="id_cargo")
	private Cargo cargo;

	//bi-directional many-to-one association to Pessoa
	@ManyToOne
	@JoinColumn(name="id_pessoa")
	private Pessoa pessoa;

	//bi-directional many-to-one association to Setor
	@ManyToOne
	@JoinColumn(name="id_setor")
	private Setor setor;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="colaborador")
	private Set<Usuario> usuarios;

	//bi-directional many-to-one association to Vendedor
	@OneToMany(mappedBy="colaborador")
	private Set<Vendedor> vendedors;

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setColaborador(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setColaborador(null);

		return usuario;
	}

	public Vendedor addVendedor(Vendedor vendedor) {
		getVendedors().add(vendedor);
		vendedor.setColaborador(this);

		return vendedor;
	}

	public Vendedor removeVendedor(Vendedor vendedor) {
		getVendedors().remove(vendedor);
		vendedor.setColaborador(null);

		return vendedor;
	}

}