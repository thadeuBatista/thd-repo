package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * The persistent class for the empresa_endereco database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="EMPRESA_ENDERECO")
@NamedQuery(name="EmpresaEndereco.findAll", query="SELECT e FROM EmpresaEndereco e")
public class EmpresaEndereco implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String bairro;

	private String cep;

	private String cidade;

	private String logradouro;

	@Column(name="municipio_ibge")
	private int municipioIbge;

	private String numero;

	private String uf;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="id_empresa")
	private Empresa empresa;


}