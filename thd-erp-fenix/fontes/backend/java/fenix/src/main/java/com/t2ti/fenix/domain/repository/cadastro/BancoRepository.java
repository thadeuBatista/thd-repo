package com.t2ti.fenix.domain.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.fenix.domain.model.cadastro.Banco;
import com.t2ti.fenix.domain.repository.cadastro.query.BancoRepositoryQuery;

public interface BancoRepository extends JpaRepository<Banco, Integer>, BancoRepositoryQuery {

	List<Banco> findFirst10ByNomeContaining(String nome);
	
}