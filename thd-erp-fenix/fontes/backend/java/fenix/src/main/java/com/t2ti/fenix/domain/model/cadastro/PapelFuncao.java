package com.t2ti.fenix.domain.model.cadastro;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * The persistent class for the papel_funcao database table.
 * 
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name="PAPEL_FUNCAO")
@NamedQuery(name="PapelFuncao.findAll", query="SELECT p FROM PapelFuncao p")
public class PapelFuncao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String habilitado;

	@Column(name="pode_alterar")
	private String podeAlterar;

	@Column(name="pode_excluir")
	private String podeExcluir;

	@Column(name="pode_inserir")
	private String podeInserir;

	//bi-directional many-to-one association to Funcao
	@ManyToOne
	@JoinColumn(name="id_funcao")
	private Funcao funcao;

	//bi-directional many-to-one association to Papel
	@ManyToOne
	@JoinColumn(name="id_papel")
	private Papel papel;
}