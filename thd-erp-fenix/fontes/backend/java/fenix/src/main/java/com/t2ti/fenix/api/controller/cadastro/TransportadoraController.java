package com.t2ti.fenix.api.controller.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.fenix.domain.exception.RecursoNaoEncontradoException;
import com.t2ti.fenix.domain.model.cadastro.Transportadora;
import com.t2ti.fenix.domain.service.cadastro.TransportadoraService;

@RestController
@RequestMapping("/transportadora")
public class TransportadoraController {

	@Autowired
	private TransportadoraService service;
	
	@GetMapping
	public List<Transportadora> listar() {
		return service.listar();
	}

	@GetMapping("/lista/{nome}")
	public List<Transportadora> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public Transportadora consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public Transportadora salvar(@RequestBody Transportadora transportadora) {
		return service.salvar(transportadora);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
