/*******************************************************************************
Title: T2Ti ERP Fenix                                                                
Description: Controller relacionado à tabela [VIEW_PESSOA_FORNECEDOR] 
                                                                                
The MIT License                                                                 
                                                                                
Copyright: Copyright (C) 2020 T2Ti.COM                                          
                                                                                
Permission is hereby granted, free of charge, to any person                     
obtaining a copy of this software and associated documentation                  
files (the "Software"), to deal in the Software without                         
restriction, including without limitation the rights to use,                    
copy, modify, merge, publish, distribute, sublicense, and/or sell               
copies of the Software, and to permit persons to whom the                       
Software is furnished to do so, subject to the following                        
conditions:                                                                     
                                                                                
The above copyright notice and this permission notice shall be                  
included in all copies or substantial portions of the Software.                 
                                                                                
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,                 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES                 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                        
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                     
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,                    
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING                    
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR                   
OTHER DEALINGS IN THE SOFTWARE.                                                 
                                                                                
       The author may be contacted at:                                          
           t2ti.com@gmail.com                                                   
                                                                                
@author Albert Eije (alberteije@gmail.com)                    
@version 1.0.0
*******************************************************************************/
package com.t2ti.fenix.api.controller.viewsDB;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.beans.BeanUtils;

import com.t2ti.fenix.api.model.viewsDB.output.ViewPessoaFornecedorDTO;
import com.t2ti.fenix.domain.model.viewsDB.ViewPessoaFornecedor;
import com.t2ti.fenix.domain.service.viewsDB.ViewPessoaFornecedorService;
import com.t2ti.fenix.api.converter.viewsDB.ViewPessoaFornecedorConverter;
import com.t2ti.fenix.api.model.filter.Filtro;



@RestController
@RequestMapping(value = "/view-pessoa-fornecedor", produces = "application/json;charset=UTF-8")
public class ViewPessoaFornecedorController {

	@Autowired
	private ViewPessoaFornecedorService service;

	@Autowired
	private ViewPessoaFornecedorConverter converter;
	
	@GetMapping
	public List<ViewPessoaFornecedorDTO> consultarLista(@RequestParam(required = false) String filter) {
		List<ViewPessoaFornecedor> listaObjeto;
		if (filter == null) {
			listaObjeto = this.service.consultarLista();
		} else {
			// define o filtro
			Filtro filtro = new Filtro(filter);
			listaObjeto = this.service.consultarLista(filtro);
		}
		
		List<ViewPessoaFornecedorDTO> listaObjetoDTO = this.converter.toCollectionModel(listaObjeto);
		return listaObjetoDTO;
	}

	@GetMapping("/{id}")
	public ViewPessoaFornecedorDTO consultarObjeto(@PathVariable Integer id) {
		ViewPessoaFornecedor objetoConsultado = this.service.consultarObjeto(id); 
		return this.converter.toModel(objetoConsultado);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ViewPessoaFornecedorDTO inserir(@RequestBody ViewPessoaFornecedorDTO objJson) {
		ViewPessoaFornecedor objeto = this.converter.toDomainObject(objJson);
		objeto = this.service.salvar(objeto);
		return this.converter.toModel(objeto);
	}

	@PutMapping("/{id}")
	public ViewPessoaFornecedorDTO alterar(@RequestBody ViewPessoaFornecedorDTO objJson, @PathVariable Integer id) {
		ViewPessoaFornecedor objeto = this.service.consultarObjeto(id);
		this.converter.toDomainObject(objJson);
		objeto = service.salvar(objeto);
		
		return this.converter.toModel(objeto);	
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable Integer id) {
		this.service.excluir(id);
	}
	
}
