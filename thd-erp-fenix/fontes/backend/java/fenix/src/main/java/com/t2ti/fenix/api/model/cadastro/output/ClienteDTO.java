package com.t2ti.fenix.api.model.cadastro.output;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO {
	
	private int id;

	private Date dataCadastro;

	private Date desde;

	private BigDecimal limiteCredito;

	private String observacao;

	private BigDecimal taxaDesconto;

	private PessoaDTO pessoa;

}
