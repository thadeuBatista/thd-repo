-- mysql workbench synchronization
-- generated: 2020-10-25 13:31
-- model: new model
-- version: 1.0
-- project: name of the project
-- author: thadeu

set @old_unique_checks=@@unique_checks, unique_checks=0;
set @old_foreign_key_checks=@@foreign_key_checks, foreign_key_checks=0;
set @old_sql_mode=@@sql_mode, sql_mode='traditional,allow_invalid_dates';

create schema if not exists `fenix` default character set utf8 ;

create table if not exists `fenix`.`pessoa` (
  `id` int(11) not null auto_increment,
  `nome` varchar(150) not null,
  `tipo` char(1) not null comment 'define o tipo de pessoa (f= física, j = jurídica)',
  `site` varchar(250) null default null,
  `email` varchar(250) null default null,
  `cliente` char(1) null default null comment 'campo para informar qual o tipo da pessoa (s = sim, n = não)',
  `fornecedor` char(1) null default null comment 'campo para informar qual o tipo da pessoa (s = sim, n = não)',
  `transportadora` char(1) null default null comment 'campo para informar qual o tipo da pessoa (s = sim, n = não)',
  `colaborador` char(1) null default null comment 'campo para informar qual o tipo da pessoa (s = sim, n = não)',
  `contador` char(1) null default null comment 'campo para informar qual o tipo da pessoa (s = sim, n = não)',
  primary key (`id`))
engine = innodb
default character set = utf8
comment = 'tabela para armazena os dados da pessoa.';

create table if not exists `fenix`.`pessoa_juridica` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `cnpj` varchar(14) null default null,
  `nome_fantasia` varchar(100) null default null,
  `inscricao_estadual` varchar(45) null default null,
  `inscricao_municipal` varchar(45) null default null,
  `data_constituicao` date null default null,
  `tipo_regime` char(1) null default null comment '1 - lucro real; 2 - lucro presumido; 3 - simples nacional',
  `crt` char(1) null default null comment 'código regime tributário\n1 - simples nacional; 2 - simples nacional - excesso de sublimite da receita bruta; 3 - regime normal',
  primary key (`id`),
  index `fk_pessoa_juridica_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_pessoa_juridica_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`pessoa_fisica` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `id_nivel_formacao` int(11) null default null,
  `id_estado_civil` int(11) null default null,
  `cpf` varchar(11) null default null,
  `rg` varchar(20) null default null,
  `orgao_rg` varchar(20) null default null,
  `data_emissao_rg` date null default null,
  `data_nascimento` date null default null,
  `sexo` char(1) null default null comment 'f = feminino\nm = masculino',
  `raca` char(1) null default null comment 'b = branco\nn = negro\np = pardo\ni = índio',
  `nacionalidade` varchar(100) null default null,
  `naturalidade` varchar(100) null default null,
  `nome_pai` varchar(200) null default null,
  `nome_mae` varchar(200) null default null,
  primary key (`id`),
  index `fk_pessoa_fisica_pessoa1_idx` (`id_pessoa` asc),
  index `fk_pessoa_fisica_nivel_formacao1_idx` (`id_nivel_formacao` asc),
  index `fk_pessoa_fisica_estado_civil1_idx` (`id_estado_civil` asc),
  constraint `fk_pessoa_fisica_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action,
  constraint `fk_pessoa_fisica_nivel_formacao1`
    foreign key (`id_nivel_formacao`)
    references `fenix`.`nivel_formacao` (`id`)
    on delete no action
    on update no action,
  constraint `fk_pessoa_fisica_estado_civil1`
    foreign key (`id_estado_civil`)
    references `fenix`.`estado_civil` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8
comment = 'tabela que armazena os dados específicos para pessoas físicas';

create table if not exists `fenix`.`cliente` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `desde` date null default null,
  `data_cadastro` date null default null,
  `taxa_desconto` decimal(18,6) null default null comment 'informa se o cliente possui uma taxa de desconto fixa a ser aplicada quando ele compra uma coisa',
  `limite_credito` decimal(18,6) null default null comment 'informa o limete de crédito disponível para o cliente.',
  `observacao` varchar(250) null default null,
  primary key (`id`),
  index `fk_cliente_pessoa_idx` (`id_pessoa` asc),
  constraint `fk_cliente_pessoa`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`fornecedor` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `desde` date null default null,
  `data_cadastro` date null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`),
  index `fk_fornecedor_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_fornecedor_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`transportadora` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `data_cadastro` date null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`),
  index `fk_transportadora_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_transportadora_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`contador` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `crc_inscricao` varchar(15) null default null,
  `crc_uf` char(2) null default null,
  primary key (`id`),
  index `fk_contador_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_contador_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`colaborador` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `id_cargo` int(11) not null,
  `id_setor` int(11) not null,
  `matricula` varchar(10) null default null,
  `data_cadastro` date null default null,
  `data_admissao` date null default null,
  `data_demissao` date null default null,
  `ctps_numero` varchar(10) null default null,
  `ctps_serie` varchar(10) null default null,
  `ctps_data_expedicao` date null default null,
  `ctps_uf` char(2) null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`),
  index `fk_colaborador_pessoa1_idx` (`id_pessoa` asc),
  index `fk_colaborador_cargo1_idx` (`id_cargo` asc),
  index `fk_colaborador_setor1_idx` (`id_setor` asc),
  constraint `fk_colaborador_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action,
  constraint `fk_colaborador_cargo1`
    foreign key (`id_cargo`)
    references `fenix`.`cargo` (`id`)
    on delete no action
    on update no action,
  constraint `fk_colaborador_setor1`
    foreign key (`id_setor`)
    references `fenix`.`setor` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`vendedor` (
  `id` int(11) not null auto_increment,
  `id_colaborador` int(11) not null,
  `comissao` decimal(18,6) null default null,
  `meta_venda` decimal(18,6) null default null,
  primary key (`id`),
  index `fk_vendedor_colaborador1_idx` (`id_colaborador` asc),
  constraint `fk_vendedor_colaborador1`
    foreign key (`id_colaborador`)
    references `fenix`.`colaborador` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`pessoa_endereco` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `logradouro` varchar(100) null default null,
  `numero` varchar(10) null default null,
  `bairro` varchar(100) null default null,
  `cidade` varchar(100) null default null,
  `uf` char(2) null default null,
  `cep` varchar(8) null default null,
  `municipio_ibge` int(11) null default null,
  `complemento` varchar(150) null default null,
  `principal` char(1) null default null comment 's=sim; n=não\n',
  `entrega` char(1) null default null comment 's=sim; n=não',
  `cobranca` char(1) null default null comment 's=sim; n=não',
  `correspondencia` char(1) null default null comment 's=sim; n=não',
  primary key (`id`),
  index `fk_pessoa_endereco_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_pessoa_endereco_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`pessoa_contato` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `nome` varchar(150) null default null,
  `email` varchar(250) null default null,
  `observacao` varchar(250) null default null comment 'informe outros tipos de contatos, por exemplo: fone, celular, linkedin',
  primary key (`id`),
  index `fk_pessoa_contato_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_pessoa_contato_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`pessoa_telefone` (
  `id` int(11) not null auto_increment,
  `id_pessoa` int(11) not null,
  `tipo` char(1) null default null,
  `numero` varchar(15) null default null comment '0 - residencial; 1 - comercial; - 2 celular; 3 - outro',
  primary key (`id`),
  index `fk_pessoa_telefone_pessoa1_idx` (`id_pessoa` asc),
  constraint `fk_pessoa_telefone_pessoa1`
    foreign key (`id_pessoa`)
    references `fenix`.`pessoa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`nivel_formacao` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`estado_civil` (
  `id` int(11) not null auto_increment,
  `nome` varchar(50) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cargo` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  `salario` decimal(18,6) null default null,
  `cbo_1994` varchar(10) null default null comment 'código brasileiro de ocupação de 1994',
  `cbo_2002` varchar(10) null default null comment 'código brasileiro de ocupação de 2002',
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`usuario` (
  `id` int(11) not null auto_increment,
  `id_colaborador` int(11) not null,
  `id_papel` int(11) not null,
  `login` varchar(45) null default null,
  `senha` varchar(45) null default null,
  `administrador` char(1) null default null comment 's=sim; n=não - se sim, recebe acesso irrestrito ao sistema.',
  `data_cadastro` date null default null,
  primary key (`id`),
  index `fk_usuario_colaborador1_idx` (`id_colaborador` asc),
  index `fk_usuario_papel1_idx` (`id_papel` asc),
  constraint `fk_usuario_colaborador1`
    foreign key (`id_colaborador`)
    references `fenix`.`colaborador` (`id`)
    on delete no action
    on update no action,
  constraint `fk_usuario_papel1`
    foreign key (`id_papel`)
    references `fenix`.`papel` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`funcao` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8
comment = 'armazena as funções da aplicação. pode ser uma tela. ex: cadastro do cliente. pode ser um botão específico. ex: calcular fluxo de caixa.';

create table if not exists `fenix`.`papel` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8
comment = 'armazena os papeis(roles) da aplicação. ex: administrativo, financeiro, cobrança, estoque, etc...';

create table if not exists `fenix`.`papel_funcao` (
  `id` int(11) not null auto_increment,
  `id_funcao` int(11) not null,
  `id_papel` int(11) not null,
  `habilitado` char(1) null default null comment 's=sim; n=não',
  `pode_inserir` char(1) null default null,
  `pode_alterar` char(1) null default null,
  `pode_excluir` char(1) null default null,
  primary key (`id`),
  index `fk_papel_funcao_funcao1_idx` (`id_funcao` asc),
  index `fk_papel_funcao_papel1_idx` (`id_papel` asc),
  constraint `fk_papel_funcao_funcao1`
    foreign key (`id_funcao`)
    references `fenix`.`funcao` (`id`)
    on delete no action
    on update no action,
  constraint `fk_papel_funcao_papel1`
    foreign key (`id_papel`)
    references `fenix`.`papel` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`empresa` (
  `id` int(11) not null auto_increment,
  `razao_social` varchar(150) null default null,
  `nome_fantasia` varchar(150) null default null,
  `cnpj` varchar(14) null default null,
  `inscricao_estadual` varchar(45) null default null,
  `inscricao_municipal` varchar(45) null default null,
  `tipo_regime` char(1) null default null comment '1 - lucro real; 2 - lucro presumido; 3 - simples nacional',
  `crt` char(1) null default null comment 'código regime tributário\n1 - simples nacional; 2 - simples nacional - excesso de sublimite da receita bruta; 3 - regime normal',
  `email` varchar(250) null default null,
  `site` varchar(250) null default null,
  `contato` varchar(100) null default null,
  `data_constituicao` date null default null,
  `tipo` char(1) null default null comment 'm=matriz; f=filial',
  `inscricao_junta_comercial` varchar(30) null default null,
  `data_insc_junta_comercial` date null default null,
  `codigo_ibge_cidade` varchar(45) null default null,
  `codigo_ibge_uf` varchar(45) null default null,
  `cei` varchar(12) null default null,
  `codigo_cnae_principal` varchar(7) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`setor` (
  `id` int(11) not null auto_increment,
  `id_empresa` int(11) not null,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`),
  index `fk_setor_empresa1_idx` (`id_empresa` asc),
  constraint `fk_setor_empresa1`
    foreign key (`id_empresa`)
    references `fenix`.`empresa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`produto` (
  `id` int(11) not null auto_increment,
  `id_produto_marca` int(11) not null,
  `id_produto_unidade` int(11) not null,
  `id_produto_subgrupo` int(11) not null,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  `gtin` varchar(14) null default null,
  `codigo_interno` varchar(50) null default null,
  `valor_compra` decimal(18,6) null default null,
  `valor_venda` decimal(18,6) null default null,
  `ncm` varchar(8) null default null comment 'nomeclatura comum do mercosul',
  `estoque_minimo` decimal(18,6) null default null,
  `estoque_maximo` decimal(18,6) null default null,
  `quantidade_estoque` decimal(18,6) null default null,
  `data_cadastro` date null default null,
  primary key (`id`),
  index `fk_produto_produto_marca1_idx` (`id_produto_marca` asc),
  index `fk_produto_produto_unidade1_idx` (`id_produto_unidade` asc),
  index `fk_produto_produto_subgrupo1_idx` (`id_produto_subgrupo` asc),
  constraint `fk_produto_produto_marca1`
    foreign key (`id_produto_marca`)
    references `fenix`.`produto_marca` (`id`)
    on delete no action
    on update no action,
  constraint `fk_produto_produto_unidade1`
    foreign key (`id_produto_unidade`)
    references `fenix`.`produto_unidade` (`id`)
    on delete no action
    on update no action,
  constraint `fk_produto_produto_subgrupo1`
    foreign key (`id_produto_subgrupo`)
    references `fenix`.`produto_subgrupo` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`produto_grupo` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`produto_subgrupo` (
  `id` int(11) not null auto_increment,
  `id_produto_grupo` int(11) not null,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`),
  index `fk_produto_subgrupo_produto_grupo1_idx` (`id_produto_grupo` asc),
  constraint `fk_produto_subgrupo_produto_grupo1`
    foreign key (`id_produto_grupo`)
    references `fenix`.`produto_grupo` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`produto_marca` (
  `id` int(11) not null auto_increment,
  `nome` varchar(100) null default null,
  `descricao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`produto_unidade` (
  `id` int(11) not null auto_increment,
  `sigla` varchar(10) null default null,
  `descricao` varchar(250) null default null,
  `pode_fracionar` char(1) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`banco` (
  `id` int(11) not null auto_increment,
  `codigo` varchar(10) null default null,
  `nome` varchar(100) null default null,
  `url` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`banco_agencia` (
  `id` int(11) not null auto_increment,
  `id_banco` int(11) not null,
  `numero` varchar(20) null default null,
  `digito` char(1) null default null,
  `nome` varchar(100) null default null,
  `telefone` varchar(15) null default null,
  `contato` varchar(100) null default null,
  `observacao` varchar(250) null default null,
  `gerente` varchar(100) null default null,
  primary key (`id`),
  index `fk_banco_agencia_banco1_idx` (`id_banco` asc),
  constraint `fk_banco_agencia_banco1`
    foreign key (`id_banco`)
    references `fenix`.`banco` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`banco_conta_caixa` (
  `id` int(11) not null auto_increment,
  `id_banco_agencia` int(11) null default null,
  `numero` varchar(15) null default null,
  `digito` char(1) null default null,
  `nome` varchar(100) null default null,
  `tipo` char(1) null default null comment 'c=corrente; p=poupança; i=investimento; x=caixa interno',
  `descricao` varchar(250) null default null comment 'descreve o objetivo da conta.',
  primary key (`id`),
  index `fk_banco_conta_caixa_banco_agencia1_idx` (`id_banco_agencia` asc),
  constraint `fk_banco_conta_caixa_banco_agencia1`
    foreign key (`id_banco_agencia`)
    references `fenix`.`banco_agencia` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cep` (
  `id` int(11) not null auto_increment,
  `cep` varchar(8) null default null,
  `logradouro` varchar(100) null default null,
  `complemento` varchar(100) null default null,
  `bairro` varchar(100) null default null,
  `municipio` varchar(100) null default null,
  `uf` char(2) null default null,
  `codigo_ibge_municipio` int(11) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`municipio` (
  `id` int(11) not null auto_increment,
  `id_uf` int(11) not null,
  `nome` varchar(100) null default null,
  `codigo_ibge` int(11) null default null,
  `codigo_receita_federal` int(11) null default null,
  `codigo_estadual` int(11) null default null,
  primary key (`id`),
  index `fk_municipio_estado1_idx` (`id_uf` asc),
  constraint `fk_municipio_estado1`
    foreign key (`id_uf`)
    references `fenix`.`uf` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`uf` (
  `id` int(11) not null auto_increment,
  `sigla` char(2) null default null,
  `nome` varchar(100) null default null,
  `codigo_ibge` int(11) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`ncm` (
  `id` int(11) not null auto_increment,
  `codigo` varchar(8) null default null,
  `descricao` varchar(1000) null default null,
  `observacao` varchar(1000) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8
comment = 'tabela de nomeclatura comum do mercosul';

create table if not exists `fenix`.`cfop` (
  `id` int(11) not null auto_increment,
  `codigo` int(11) null default null,
  `descricao` varchar(1000) null default null,
  `aplicacao` varchar(1000) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8
comment = 'tabela para guardar códigos de emissão de notas fiscais';

create table if not exists `fenix`.`cst_icms` (
  `id` int(11) not null auto_increment,
  `codigo` char(2) null default null,
  `descricao` varchar(250) null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`csosn` (
  `id` int(11) not null auto_increment,
  `codigo` char(3) null default null,
  `descricao` varchar(250) null default null,
  `observacao` varchar(1000) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`empresa_endereco` (
  `id` int(11) not null auto_increment,
  `logradouro` varchar(100) null default null,
  `numero` varchar(10) null default null,
  `bairro` varchar(100) null default null,
  `cidade` varchar(100) null default null,
  `uf` char(2) null default null,
  `cep` varchar(8) null default null,
  `municipio_ibge` int(11) null default null,
  `id_empresa` int(11) not null,
  `complemento` varchar(150) null default null,
  `principal` char(1) null default null,
  `entrega` char(1) null default null,
  `cobranca` char(1) null default null,
  `correspondencia` char(1) null default null,
  primary key (`id`),
  index `fk_empresa_endereco_empresa1_idx` (`id_empresa` asc),
  constraint `fk_empresa_endereco_empresa1`
    foreign key (`id_empresa`)
    references `fenix`.`empresa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`empresa_contato` (
  `id` int(11) not null auto_increment,
  `nome` varchar(150) null default null,
  `email` varchar(250) null default null,
  `observacao` varchar(250) null default null comment 'informe outros tipos de contatos, por exemplo: fone, celular, linkedin',
  `id_empresa` int(11) not null,
  primary key (`id`),
  index `fk_empresa_contato_empresa1_idx` (`id_empresa` asc),
  constraint `fk_empresa_contato_empresa1`
    foreign key (`id_empresa`)
    references `fenix`.`empresa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`empresa_telefone` (
  `id` int(11) not null auto_increment,
  `tipo` char(1) null default null,
  `numero` varchar(15) null default null comment '0 - residencial; 1 - comercial; - 2 celular; 3 - outro',
  `id_empresa` int(11) not null,
  primary key (`id`),
  index `fk_empresa_telefone_empresa1_idx` (`id_empresa` asc),
  constraint `fk_empresa_telefone_empresa1`
    foreign key (`id_empresa`)
    references `fenix`.`empresa` (`id`)
    on delete no action
    on update no action)
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cst_ipi` (
  `id` int(11) not null auto_increment,
  `codigo` char(2) null default null,
  `descricao` varchar(250) null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cst_cofins` (
  `id` int(11) not null auto_increment,
  `codigo` char(2) null default null,
  `descricao` varchar(250) null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cst_pis` (
  `id` int(11) not null auto_increment,
  `codigo` char(2) null default null,
  `descricao` varchar(250) null default null,
  `observacao` varchar(250) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cnae` (
  `id` int(11) not null auto_increment,
  `denominacao` varchar(250) null default null,
  `codigo` varchar(10) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;

create table if not exists `fenix`.`cbo` (
  `id` int(11) not null,
  `codigo` varchar(10) null default null,
  `nome` varchar(150) null default null,
  primary key (`id`))
engine = innodb
default character set = utf8;


set sql_mode=@old_sql_mode;
set foreign_key_checks=@old_foreign_key_checks;
set unique_checks=@old_unique_checks;

