package com.t2ti.siscom.services.financeiro;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinExtratoContaBanco;
import com.webcohesion.ofx4j.domain.data.MessageSetType;
import com.webcohesion.ofx4j.domain.data.ResponseEnvelope;
import com.webcohesion.ofx4j.domain.data.banking.BankingResponseMessageSet;
import com.webcohesion.ofx4j.domain.data.common.Transaction;
import com.webcohesion.ofx4j.io.AggregateUnmarshaller;

@Service
public class ImportaOFXService {

	public List<FinExtratoContaBanco> importa(InputStream arquivoOFX) throws Exception {
		List<FinExtratoContaBanco> extrato = new ArrayList<>();

		AggregateUnmarshaller<ResponseEnvelope> unmarshaller = new AggregateUnmarshaller<ResponseEnvelope>(
				ResponseEnvelope.class);

		ResponseEnvelope envelope = unmarshaller.unmarshal(arquivoOFX);
		
		BankingResponseMessageSet bank = (BankingResponseMessageSet) envelope.getMessageSet(MessageSetType.banking);

		List<Transaction> transactions = bank.getStatementResponses().get(0).getMessage().getTransactionList().getTransactions();
		
		Date dataExtrato = bank.getStatementResponses().get(0).getMessage().getTransactionList().getStart();
		
		String mesAno = new SimpleDateFormat("MM/yyyy").format(dataExtrato);
		
		for (Transaction t : transactions) {
			FinExtratoContaBanco item = new FinExtratoContaBanco();
			item.setDataMovimento(t.getDatePosted());
			item.setDocumento(t.getCheckNumber());
			item.setHistorico(t.getMemo());
			item.setValor(t.getBigDecimalAmount());
			item.setMesAno(mesAno);
			
			extrato.add(item);
		}
		
		return extrato;
	}

}
