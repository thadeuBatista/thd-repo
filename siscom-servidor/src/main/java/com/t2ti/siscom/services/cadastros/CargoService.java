package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Cargo;
import com.t2ti.siscom.repository.cadastros.CargoRepository;

@Service
public class CargoService {

	@Autowired
	private CargoRepository repository;
	
	public List<Cargo> listar() {
		return repository.findAll();
	}

	public List<Cargo> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Cargo consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Cargo salvar(Cargo cargo) {
		return repository.save(cargo);
	}
	
	public void excluir(Integer id) {
		Cargo cargo = new Cargo();
		cargo.setId(id);
		repository.delete(cargo);
	}
	
}
