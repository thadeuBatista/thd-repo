package com.t2ti.siscom.services.estoque;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Produto;
import com.t2ti.siscom.model.estoque.EstoqueReajusteCabecalho;
import com.t2ti.siscom.model.estoque.EstoqueReajusteDetalhe;
import com.t2ti.siscom.repository.cadastros.ProdutoRepository;
import com.t2ti.siscom.repository.estoque.EstoqueReajusteCabecalhoRepository;

@Service
public class EstoqueReajusteCabecalhoService {

	@Autowired
	private EstoqueReajusteCabecalhoRepository repository;
	@Autowired
	private ProdutoRepository repositoryProduto;
	
	public List<EstoqueReajusteCabecalho> listar() {
		return repository.findAll();
	}
	
	public EstoqueReajusteCabecalho consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public EstoqueReajusteCabecalho salvar(EstoqueReajusteCabecalho estoqueReajusteCabecalho) {
		for (EstoqueReajusteDetalhe d : estoqueReajusteCabecalho.getListaEstoqueReajusteDetalhe()) {
			d.setEstoqueReajusteCabecalho(estoqueReajusteCabecalho);
			d.setValorOriginal(d.getProduto().getValorVenda());
			
			BigDecimal reajuste = d.getProduto().getValorVenda().multiply(estoqueReajusteCabecalho.getPorcentagem());

			Produto produto = d.getProduto();
			if (estoqueReajusteCabecalho.getTipoReajuste().equals("A")) {
				produto.setValorVenda(produto.getValorVenda().add(reajuste));
			} else {
				produto.setValorVenda(produto.getValorVenda().subtract(reajuste));
			}
			repositoryProduto.save(produto);
			d.setValorReajuste(produto.getValorVenda());
		}
		return repository.save(estoqueReajusteCabecalho);
	}
	
	public void excluir(Integer id) {
		EstoqueReajusteCabecalho estoqueReajusteCabecalho = new EstoqueReajusteCabecalho();
		estoqueReajusteCabecalho.setId(id);
		repository.delete(estoqueReajusteCabecalho);
	}
	
}
