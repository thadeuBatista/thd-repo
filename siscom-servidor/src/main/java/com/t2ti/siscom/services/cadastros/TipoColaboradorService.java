package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.TipoColaborador;
import com.t2ti.siscom.repository.cadastros.TipoColaboradorRepository;

@Service
public class TipoColaboradorService {

	@Autowired
	private TipoColaboradorRepository repository;
	
	public List<TipoColaborador> listar() {
		return repository.findAll();
	}
	
	public List<TipoColaborador> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public TipoColaborador consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public TipoColaborador salvar(TipoColaborador tipoColaborador) {
		return repository.save(tipoColaborador);
	}
	
	public void excluir(Integer id) {
		TipoColaborador tipoColaborador = new TipoColaborador();
		tipoColaborador.setId(id);
		repository.delete(tipoColaborador);
	}
	
}
