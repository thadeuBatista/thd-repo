package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Transportadora;
import com.t2ti.siscom.repository.cadastros.TransportadoraRepository;

@Service
public class TransportadoraService {

	@Autowired
	private TransportadoraRepository repository;
	
	public List<Transportadora> listar() {
		return repository.findAll();
	}

	public List<Transportadora> listar(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}
	
	public Transportadora consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Transportadora salvar(Transportadora transportadora) {
		return repository.save(transportadora);
	}
	
	public void excluir(Integer id) {
		Transportadora transportadora = new Transportadora();
		transportadora.setId(id);
		repository.delete(transportadora);
	}
	
}
