package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinStatusParcela;
import com.t2ti.siscom.repository.financeiro.FinStatusParcelaRepository;

@Service
public class FinStatusParcelaService {

	@Autowired
	private FinStatusParcelaRepository repository;
	
	public List<FinStatusParcela> listar() {
		return repository.findAll();
	}
	
	public List<FinStatusParcela> listar(String nome) {
		return repository.findFirst10ByDescricaoContaining(nome);
	}
	
	public FinStatusParcela consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinStatusParcela salvar(FinStatusParcela finStatusParcela) {
		return repository.save(finStatusParcela);
	}
	
	public void excluir(Integer id) {
		FinStatusParcela finStatusParcela = new FinStatusParcela();
		finStatusParcela.setId(id);
		repository.delete(finStatusParcela);
	}
	
}
