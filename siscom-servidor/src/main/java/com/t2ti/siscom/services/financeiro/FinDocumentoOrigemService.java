package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinDocumentoOrigem;
import com.t2ti.siscom.repository.financeiro.FinDocumentoOrigemRepository;

@Service
public class FinDocumentoOrigemService {

	@Autowired
	private FinDocumentoOrigemRepository repository;
	
	public List<FinDocumentoOrigem> listar() {
		return repository.findAll();
	}
	
	public List<FinDocumentoOrigem> listar(String nome) {
		return repository.findFirst10BySiglaDocumentoContaining(nome);
	}
	
	public FinDocumentoOrigem consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinDocumentoOrigem salvar(FinDocumentoOrigem finDocumentoOrigem) {
		return repository.save(finDocumentoOrigem);
	}
	
	public void excluir(Integer id) {
		FinDocumentoOrigem finDocumentoOrigem = new FinDocumentoOrigem();
		finDocumentoOrigem.setId(id);
		repository.delete(finDocumentoOrigem);
	}
	
}
