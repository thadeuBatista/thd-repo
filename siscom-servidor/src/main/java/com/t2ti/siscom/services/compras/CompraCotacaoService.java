package com.t2ti.siscom.services.compras;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.compras.CompraCotacao;
import com.t2ti.siscom.model.compras.CompraCotacaoDetalhe;
import com.t2ti.siscom.model.compras.CompraFornecedorCotacao;
import com.t2ti.siscom.model.compras.CompraReqCotacaoDetalhe;
import com.t2ti.siscom.repository.compras.CompraCotacaoRepository;

@Service
public class CompraCotacaoService {

	@Autowired
	private CompraCotacaoRepository repository;
	
	public List<CompraCotacao> listar() {
		return repository.findAll();
	}
	
	public CompraCotacao consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public CompraCotacao salvar(CompraCotacao compraCotacao) {
		for (CompraFornecedorCotacao f : compraCotacao.getListaCompraFornecedorCotacao()) {
			f.setCompraCotacao(compraCotacao);
			if (f.getListaCompraCotacaoDetalhe() == null) {
				f.setListaCompraCotacaoDetalhe(new HashSet<>());
			}
			for (CompraReqCotacaoDetalhe d : compraCotacao.getListaCompraReqCotacaoDetalhe()) {
				d.setCompraCotacao(compraCotacao);
				
				CompraCotacaoDetalhe compraCotacaoDetalhe = new CompraCotacaoDetalhe();
				compraCotacaoDetalhe.setCompraFornecedorCotacao(f);
				compraCotacaoDetalhe.setProduto(d.getCompraRequisicaoDetalhe().getProduto());
				compraCotacaoDetalhe.setQuantidade(d.getQuantidadeCotada());
				
				f.getListaCompraCotacaoDetalhe().add(compraCotacaoDetalhe);
			}
		}
		return repository.save(compraCotacao);
	}
	
	public void excluir(Integer id) {
		CompraCotacao compraCotacao = new CompraCotacao();
		compraCotacao.setId(id);
		repository.delete(compraCotacao);
	}
	
}
