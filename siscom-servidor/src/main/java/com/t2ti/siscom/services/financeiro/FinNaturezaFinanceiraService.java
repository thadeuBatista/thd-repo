package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinNaturezaFinanceira;
import com.t2ti.siscom.repository.financeiro.FinNaturezaFinanceiraRepository;

@Service
public class FinNaturezaFinanceiraService {

	@Autowired
	private FinNaturezaFinanceiraRepository repository;
	
	public List<FinNaturezaFinanceira> listar() {
		return repository.findAll();
	}
	
	public List<FinNaturezaFinanceira> listar(String nome) {
		return repository.findFirst10ByCodigoContaining(nome);
	}
	
	public FinNaturezaFinanceira consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinNaturezaFinanceira salvar(FinNaturezaFinanceira finNaturezaFinanceira) {
		return repository.save(finNaturezaFinanceira);
	}
	
	public void excluir(Integer id) {
		FinNaturezaFinanceira finNaturezaFinanceira = new FinNaturezaFinanceira();
		finNaturezaFinanceira.setId(id);
		repository.delete(finNaturezaFinanceira);
	}
	
}
