package com.t2ti.siscom.services.vendas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.vendas.VendaCondicoesPagamento;
import com.t2ti.siscom.repository.vendas.VendaCondicoesPagamentoRepository;

@Service
public class VendaCondicoesPagamentoService {

	@Autowired
	private VendaCondicoesPagamentoRepository repository;
	
	public List<VendaCondicoesPagamento> listar() {
		return repository.findAll();
	}
	
	public List<VendaCondicoesPagamento> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public VendaCondicoesPagamento consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public VendaCondicoesPagamento salvar(VendaCondicoesPagamento vendaCondicoesPagamento) {
		return repository.save(vendaCondicoesPagamento);
	}
	
	public void excluir(Integer id) {
		VendaCondicoesPagamento vendaCondicoesPagamento = new VendaCondicoesPagamento();
		vendaCondicoesPagamento.setId(id);
		repository.delete(vendaCondicoesPagamento);
	}
	
}
