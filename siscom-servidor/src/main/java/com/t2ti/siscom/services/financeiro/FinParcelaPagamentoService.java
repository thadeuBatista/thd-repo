package com.t2ti.siscom.services.financeiro;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinParcelaPagamento;
import com.t2ti.siscom.model.financeiro.FinParcelaPagar;
import com.t2ti.siscom.repository.financeiro.FinParcelaPagamentoRepository;

@Service
public class FinParcelaPagamentoService {

	@Autowired
	private FinParcelaPagamentoRepository repository;
	
	public List<FinParcelaPagar> listar() {
		return repository.findAll();
	}
	
	public FinParcelaPagar consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinParcelaPagar salvar(FinParcelaPagar finParcelaPagamento) {
		Set<FinParcelaPagamento> pagamentos = finParcelaPagamento.getListaFinParcelaPagamento();
		
		finParcelaPagamento = repository.findById(finParcelaPagamento.getId()).get();
		finParcelaPagamento.getListaFinParcelaPagamento().addAll(pagamentos);
		
		for (FinParcelaPagamento p : finParcelaPagamento.getListaFinParcelaPagamento()) {
			p.setFinParcelaPagar(finParcelaPagamento);
		}
		return repository.save(finParcelaPagamento);
	}
	
	public void excluir(Integer id) {
		FinParcelaPagar finParcelaPagamento = new FinParcelaPagar();
		finParcelaPagamento.setId(id);
		repository.delete(finParcelaPagamento);
	}
	
}
