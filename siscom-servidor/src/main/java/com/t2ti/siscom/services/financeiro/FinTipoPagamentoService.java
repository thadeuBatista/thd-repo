package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinTipoPagamento;
import com.t2ti.siscom.repository.financeiro.FinTipoPagamentoRepository;

@Service
public class FinTipoPagamentoService {

	@Autowired
	private FinTipoPagamentoRepository repository;
	
	public List<FinTipoPagamento> listar() {
		return repository.findAll();
	}
	
	public List<FinTipoPagamento> listar(String nome) {
		return repository.findFirst10ByDescricaoContaining(nome);
	}
	
	public FinTipoPagamento consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinTipoPagamento salvar(FinTipoPagamento finTipoPagamento) {
		return repository.save(finTipoPagamento);
	}
	
	public void excluir(Integer id) {
		FinTipoPagamento finTipoPagamento = new FinTipoPagamento();
		finTipoPagamento.setId(id);
		repository.delete(finTipoPagamento);
	}
	
}
