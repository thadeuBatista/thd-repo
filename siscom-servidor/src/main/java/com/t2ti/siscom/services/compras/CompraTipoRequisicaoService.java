package com.t2ti.siscom.services.compras;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.compras.CompraTipoRequisicao;
import com.t2ti.siscom.repository.compras.CompraTipoRequisicaoRepository;

@Service
public class CompraTipoRequisicaoService {

	@Autowired
	private CompraTipoRequisicaoRepository repository;
	
	public List<CompraTipoRequisicao> listar() {
		return repository.findAll();
	}
	
	public List<CompraTipoRequisicao> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public CompraTipoRequisicao consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public CompraTipoRequisicao salvar(CompraTipoRequisicao compraTipoRequisicao) {
		return repository.save(compraTipoRequisicao);
	}
	
	public void excluir(Integer id) {
		CompraTipoRequisicao compraTipoRequisicao = new CompraTipoRequisicao();
		compraTipoRequisicao.setId(id);
		repository.delete(compraTipoRequisicao);
	}
	
}
