package com.t2ti.siscom.services.financeiro;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinChequeEmitido;
import com.t2ti.siscom.model.financeiro.FinParcelaPagamento;
import com.t2ti.siscom.model.financeiro.FinParcelaPagar;
import com.t2ti.siscom.model.financeiro.FinTipoPagamento;
import com.t2ti.siscom.repository.financeiro.FinChequeEmitidoRepository;
import com.t2ti.siscom.repository.financeiro.FinParcelaPagamentoRepository;

@Service
public class FinChequeEmitidoService {

	@Autowired
	private FinChequeEmitidoRepository repository;
	@Autowired
	private FinParcelaPagamentoRepository pagamentoRepository;
	
	public List<FinChequeEmitido> listar() {
		return repository.findAll();
	}
	
	public FinChequeEmitido consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinChequeEmitido salvar(FinChequeEmitido finChequeEmitido) {
		FinTipoPagamento tipoPagamento = new FinTipoPagamento();
		tipoPagamento.setId(2);
		
		finChequeEmitido = repository.save(finChequeEmitido);
		
		for (FinParcelaPagar p : finChequeEmitido.getListaFinParcelaPagar()) {
			p = pagamentoRepository.findById(p.getId()).get();
			
			FinParcelaPagamento pagamento = new FinParcelaPagamento();
			pagamento.setContaCaixa(p.getContaCaixa());
			pagamento.setFinChequeEmitido(finChequeEmitido);
			pagamento.setFinParcelaPagar(p);
			pagamento.setFinTipoPagamento(tipoPagamento);
			pagamento.setValorPago(p.getValor());
			
			if (p.getListaFinParcelaPagamento() == null) {
				p.setListaFinParcelaPagamento(new HashSet<>());
			}
			p.getListaFinParcelaPagamento().add(pagamento);
			pagamentoRepository.save(p);
		}
		
		return finChequeEmitido;
	}
	
	public void excluir(Integer id) {
		FinChequeEmitido finChequeEmitido = new FinChequeEmitido();
		finChequeEmitido.setId(id);
		repository.delete(finChequeEmitido);
	}
	
}
