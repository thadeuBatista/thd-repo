package com.t2ti.siscom.services.vendas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.vendas.VendaOrcamentoCabecalho;
import com.t2ti.siscom.model.vendas.VendaOrcamentoDetalhe;
import com.t2ti.siscom.repository.vendas.VendaOrcamentoCabecalhoRepository;

@Service
public class VendaOrcamentoCabecalhoService {

	@Autowired
	private VendaOrcamentoCabecalhoRepository repository;
	
	public List<VendaOrcamentoCabecalho> listar() {
		return repository.findAll();
	}
	
	public List<VendaOrcamentoCabecalho> listar(String nome) {
		return repository.findFirst10ByCodigoContaining(nome);
	}
	
	public VendaOrcamentoCabecalho consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public VendaOrcamentoCabecalho salvar(VendaOrcamentoCabecalho vendaOrcamentoCabecalho) {
		for (VendaOrcamentoDetalhe d : vendaOrcamentoCabecalho.getListaVendaOrcamentoDetalhe()) {
			d.setVendaOrcamentoCabecalho(vendaOrcamentoCabecalho);
		}
		return repository.save(vendaOrcamentoCabecalho);
	}
	
	public void excluir(Integer id) {
		VendaOrcamentoCabecalho vendaOrcamentoCabecalho = new VendaOrcamentoCabecalho();
		vendaOrcamentoCabecalho.setId(id);
		repository.delete(vendaOrcamentoCabecalho);
	}
	
}
