package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.t2ti.siscom.model.cadastros.ContaCaixa;
import com.t2ti.siscom.model.financeiro.FinExtratoContaBanco;
import com.t2ti.siscom.repository.financeiro.FinExtratoContaBancoRepository;

@Service
public class FinExtratoContaBancoService {

	@Autowired
	private FinExtratoContaBancoRepository repository;
	@Autowired
	private ImportaOFXService ofxService;
	
	public List<FinExtratoContaBanco> listar(Integer idContaCaixa, String mesAno) {
		ContaCaixa contaCaixa = new ContaCaixa();
		contaCaixa.setId(idContaCaixa);
		
		mesAno = mesAno.substring(0, 2) + "/" + mesAno.substring(2);
		System.out.println(mesAno);
		
		return repository.findByContaCaixaAndMesAno(contaCaixa, mesAno);
	}
	
	public FinExtratoContaBanco consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinExtratoContaBanco salvar(FinExtratoContaBanco finExtratoContaBanco) {
		return repository.save(finExtratoContaBanco);
	}
	
	public void excluir(Integer id) {
		FinExtratoContaBanco finExtratoContaBanco = new FinExtratoContaBanco();
		finExtratoContaBanco.setId(id);
		repository.delete(finExtratoContaBanco);
	}
	
	public void uploadExtrato(MultipartFile file, Integer idContaCaixa) throws Exception {
		List<FinExtratoContaBanco> extrato = ofxService.importa(file.getInputStream());
		ContaCaixa contaCaixa = new ContaCaixa();
		contaCaixa.setId(idContaCaixa);
		
		for(FinExtratoContaBanco e : extrato) {
			e.setContaCaixa(contaCaixa);
		}
		
		repository.saveAll(extrato);
	}	
	
}
