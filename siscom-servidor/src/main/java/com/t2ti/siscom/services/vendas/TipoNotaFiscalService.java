package com.t2ti.siscom.services.vendas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.vendas.TipoNotaFiscal;
import com.t2ti.siscom.repository.vendas.TipoNotaFiscalRepository;

@Service
public class TipoNotaFiscalService {

	@Autowired
	private TipoNotaFiscalRepository repository;
	
	public List<TipoNotaFiscal> listar() {
		return repository.findAll();
	}
	
	public List<TipoNotaFiscal> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public TipoNotaFiscal consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public TipoNotaFiscal salvar(TipoNotaFiscal tipoNotaFiscal) {
		return repository.save(tipoNotaFiscal);
	}
	
	public void excluir(Integer id) {
		TipoNotaFiscal tipoNotaFiscal = new TipoNotaFiscal();
		tipoNotaFiscal.setId(id);
		repository.delete(tipoNotaFiscal);
	}
	
}
