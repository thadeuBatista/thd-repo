package com.t2ti.siscom.services.vendas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.vendas.VendaCabecalho;
import com.t2ti.siscom.model.vendas.VendaComissao;
import com.t2ti.siscom.model.vendas.VendaDetalhe;
import com.t2ti.siscom.repository.vendas.VendaCabecalhoRepository;
import com.t2ti.siscom.repository.vendas.VendaComissaoRepository;

@Service
public class VendaCabecalhoService {

	@Autowired
	private VendaCabecalhoRepository repository;
	@Autowired
	private VendaComissaoRepository repositoryComissao;
	
	public List<VendaCabecalho> listar() {
		return repository.findAll();
	}
	
	public VendaCabecalho consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public VendaCabecalho salvar(VendaCabecalho vendaCabecalho) {
		for (VendaDetalhe d : vendaCabecalho.getListaVendaDetalhe()) {
			d.setVendaCabecalho(vendaCabecalho);
		}
		boolean insereComissao = false;
		if(vendaCabecalho.getId() == null) {
			insereComissao = true;
		}
		
		vendaCabecalho = repository.save(vendaCabecalho);
		
		if(insereComissao) {
			VendaComissao comissao = new VendaComissao();
			comissao.setVendaCabecalho(vendaCabecalho);
			comissao.setVendedor(vendaCabecalho.getVendedor());
			comissao.setValorVenda(vendaCabecalho.getValorTotal());
			comissao.setTipoContabil("C");
			comissao.setSituacao("A");
			comissao.setDataLancamento(vendaCabecalho.getDataVenda());
			comissao.setValorComissao(vendaCabecalho.getValorTotal().multiply(vendaCabecalho.getVendedor().getComissao()));
			
			repositoryComissao.save(comissao);
		}

		return vendaCabecalho;
	}
	
	public void excluir(Integer id) {
		VendaCabecalho vendaCabecalho = new VendaCabecalho();
		vendaCabecalho.setId(id);
		repository.delete(vendaCabecalho);
	}
	
}
