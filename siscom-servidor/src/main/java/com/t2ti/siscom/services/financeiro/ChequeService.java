package com.t2ti.siscom.services.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.Cheque;
import com.t2ti.siscom.repository.financeiro.ChequeRepository;

@Service
public class ChequeService {

	@Autowired
	private ChequeRepository repository;
	
	public List<Cheque> listar() {
		return repository.findAll();
	}
	
	public List<Cheque> listar(Integer numero) {
		return repository.findByNumero(numero);
	}
	
	public Cheque consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Cheque salvar(Cheque cheque) {
		return repository.save(cheque);
	}
	
	public void excluir(Integer id) {
		Cheque cheque = new Cheque();
		cheque.setId(id);
		repository.delete(cheque);
	}
	
}
