package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Fornecedor;
import com.t2ti.siscom.repository.cadastros.FornecedorRepository;

@Service
public class FornecedorService {

	@Autowired
	private FornecedorRepository repository;
	
	public List<Fornecedor> listar() {
		return repository.findAll();
	}
	
	public List<Fornecedor> listar(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}
	
	public Fornecedor consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Fornecedor salvar(Fornecedor fornecedor) {
		return repository.save(fornecedor);
	}
	
	public void excluir(Integer id) {
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setId(id);
		repository.delete(fornecedor);
	}
	
}
