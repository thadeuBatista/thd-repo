package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.ContaCaixa;
import com.t2ti.siscom.repository.cadastros.ContaCaixaRepository;

@Service
public class ContaCaixaService {

	@Autowired
	private ContaCaixaRepository repository;
	
	public List<ContaCaixa> listar() {
		return repository.findAll();
	}

	public List<ContaCaixa> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public ContaCaixa consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public ContaCaixa salvar(ContaCaixa contaCaixa) {
		return repository.save(contaCaixa);
	}
	
	public void excluir(Integer id) {
		ContaCaixa contaCaixa = new ContaCaixa();
		contaCaixa.setId(id);
		repository.delete(contaCaixa);
	}
	
}
