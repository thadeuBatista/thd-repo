package com.t2ti.siscom.services.financeiro;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.financeiro.FinLancamentoPagar;
import com.t2ti.siscom.model.financeiro.FinParcelaPagar;
import com.t2ti.siscom.model.financeiro.FinStatusParcela;
import com.t2ti.siscom.repository.financeiro.FinLancamentoPagarRepository;

@Service
public class FinLancamentoPagarService {

	@Autowired
	private FinLancamentoPagarRepository repository;
	
	public List<FinLancamentoPagar> listar() {
		return repository.findAll();
	}
	
	public FinLancamentoPagar consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public FinLancamentoPagar salvar(FinLancamentoPagar finLancamentoPagar) {
		FinStatusParcela statusParcela = new FinStatusParcela();
		statusParcela.setId(1);
		
		BigDecimal valorParcela = finLancamentoPagar.getValorTotal().divide(BigDecimal.valueOf(finLancamentoPagar.getQuantidadeParcela()));
		
		Calendar dataAtual = Calendar.getInstance();
		Calendar primeiroVencimento = Calendar.getInstance();
		primeiroVencimento.setTime(finLancamentoPagar.getPrimeiroVencimento());
		
		if (finLancamentoPagar.getListaFinParcelaPagar() == null) {
			finLancamentoPagar.setListaFinParcelaPagar(new HashSet<>());
		}
		
		for (int i = 0; i < finLancamentoPagar.getQuantidadeParcela(); i++) {
			FinParcelaPagar parcelaPagar = new FinParcelaPagar();
			parcelaPagar.setContaCaixa(finLancamentoPagar.getContaCaixa());
			parcelaPagar.setFinLancamentoPagar(finLancamentoPagar);
			parcelaPagar.setFinStatusParcela(statusParcela);
			parcelaPagar.setDataEmissao(dataAtual.getTime());
			parcelaPagar.setNumeroParcela(i + 1);
			parcelaPagar.setValor(valorParcela);
			if (i == 0) {
				parcelaPagar.setDataVencimento(primeiroVencimento.getTime());
			} else {
				primeiroVencimento.add(Calendar.DAY_OF_MONTH, finLancamentoPagar.getIntervaloEntreParcelas());
				parcelaPagar.setDataVencimento(primeiroVencimento.getTime());
			}

			finLancamentoPagar.getListaFinParcelaPagar().add(parcelaPagar);
		}
		
		return repository.save(finLancamentoPagar);
	}
	
	public void excluir(Integer id) {
		FinLancamentoPagar finLancamentoPagar = new FinLancamentoPagar();
		finLancamentoPagar.setId(id);
		repository.delete(finLancamentoPagar);
	}
	
}
