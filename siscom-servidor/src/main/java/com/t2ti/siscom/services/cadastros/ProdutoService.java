package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Produto;
import com.t2ti.siscom.repository.cadastros.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository repository;
	
	public List<Produto> listar() {
		return repository.findAll();
	}
	
	public List<Produto> listar(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Produto consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Produto salvar(Produto produto) {
		return repository.save(produto);
	}
	
	public void excluir(Integer id) {
		Produto produto = new Produto();
		produto.setId(id);
		repository.delete(produto);
	}
	
}
