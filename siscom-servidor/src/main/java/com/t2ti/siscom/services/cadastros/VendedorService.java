package com.t2ti.siscom.services.cadastros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.cadastros.Vendedor;
import com.t2ti.siscom.repository.cadastros.VendedorRepository;

@Service
public class VendedorService {

	@Autowired
	private VendedorRepository repository;
	
	public List<Vendedor> listar() {
		return repository.findAll();
	}

	public List<Vendedor> listar(String nome) {
		return repository.findFirst10ByColaboradorPessoaNomeContaining(nome);
	}
	
	public Vendedor consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public Vendedor salvar(Vendedor vendedor) {
		return repository.save(vendedor);
	}
	
	public void excluir(Integer id) {
		Vendedor vendedor = new Vendedor();
		vendedor.setId(id);
		repository.delete(vendedor);
	}
	
}
