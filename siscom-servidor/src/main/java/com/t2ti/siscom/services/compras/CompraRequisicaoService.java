package com.t2ti.siscom.services.compras;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.compras.CompraRequisicao;
import com.t2ti.siscom.model.compras.CompraRequisicaoDetalhe;
import com.t2ti.siscom.repository.compras.CompraRequisicaoRepository;

@Service
public class CompraRequisicaoService {

	@Autowired
	private CompraRequisicaoRepository repository;
	
	public List<CompraRequisicao> listar() {
		return repository.findAll();
	}
	
	public CompraRequisicao consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public CompraRequisicao salvar(CompraRequisicao compraRequisicao) {
		for (CompraRequisicaoDetalhe d : compraRequisicao.getListaCompraRequisicaoDetalhe()) {
			d.setCompraRequisicao(compraRequisicao);
		}
		return repository.save(compraRequisicao);
	}
	
	public void excluir(Integer id) {
		CompraRequisicao compraRequisicao = new CompraRequisicao();
		compraRequisicao.setId(id);
		repository.delete(compraRequisicao);
	}
	
}
