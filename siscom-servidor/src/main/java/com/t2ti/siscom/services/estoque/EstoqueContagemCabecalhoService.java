package com.t2ti.siscom.services.estoque;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.t2ti.siscom.model.estoque.EstoqueContagemCabecalho;
import com.t2ti.siscom.model.estoque.EstoqueContagemDetalhe;
import com.t2ti.siscom.repository.estoque.EstoqueContagemCabecalhoRepository;

@Service
public class EstoqueContagemCabecalhoService {

	@Autowired
	private EstoqueContagemCabecalhoRepository repository;
	
	public List<EstoqueContagemCabecalho> listar() {
		return repository.findAll();
	}
	
	public EstoqueContagemCabecalho consultarObjeto(Integer id) {
		return repository.findById(id).get();
	}
	
	public EstoqueContagemCabecalho salvar(EstoqueContagemCabecalho estoqueContagemCabecalho) {
		for (EstoqueContagemDetalhe d : estoqueContagemCabecalho.getListaEstoqueContagemDetalhe()) {
			d.setEstoqueContagemCabecalho(estoqueContagemCabecalho);
			d.setQuantidadeSistema(d.getProduto().getQuantidadeEstoque());
			
			d.setAcuracidade(d.getQuantidadeContada().divide(d.getQuantidadeSistema()));
			d.setDivergencia(d.getQuantidadeContada().subtract(d.getQuantidadeSistema()));
		}
		return repository.save(estoqueContagemCabecalho);
	}
	
	public void excluir(Integer id) {
		EstoqueContagemCabecalho estoqueContagemCabecalho = new EstoqueContagemCabecalho();
		estoqueContagemCabecalho.setId(id);
		repository.delete(estoqueContagemCabecalho);
	}
	
}
