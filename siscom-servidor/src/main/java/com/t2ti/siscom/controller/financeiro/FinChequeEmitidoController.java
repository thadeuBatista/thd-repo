package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.FinChequeEmitido;
import com.t2ti.siscom.services.financeiro.FinChequeEmitidoService;

@RestController
@RequestMapping("/financeiro/emissao-cheque")
public class FinChequeEmitidoController {

	@Autowired
	private FinChequeEmitidoService service;
	
	@GetMapping
	public List<FinChequeEmitido> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public FinChequeEmitido consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public FinChequeEmitido salvar(@RequestBody FinChequeEmitido finChequeEmitido) {
		return service.salvar(finChequeEmitido);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
