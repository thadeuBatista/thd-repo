package com.t2ti.siscom.controller.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.compras.CompraCotacao;
import com.t2ti.siscom.services.compras.CompraCotacaoService;

@RestController
@RequestMapping("/compra/cotacao")
public class CompraCotacaoController {

	@Autowired
	private CompraCotacaoService service;
	
	@GetMapping
	public List<CompraCotacao> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public CompraCotacao consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public CompraCotacao salvar(@RequestBody CompraCotacao compraCotacao) {
		return service.salvar(compraCotacao);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
