package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.FinTipoPagamento;
import com.t2ti.siscom.services.financeiro.FinTipoPagamentoService;

@RestController
@RequestMapping("/financeiro/tipo-pagamento")
public class FinTipoPagamentoController {

	@Autowired
	private FinTipoPagamentoService service;
	
	@GetMapping
	public List<FinTipoPagamento> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<FinTipoPagamento> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public FinTipoPagamento consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public FinTipoPagamento salvar(@RequestBody FinTipoPagamento finTipoPagamento) {
		return service.salvar(finTipoPagamento);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
