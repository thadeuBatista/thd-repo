package com.t2ti.siscom.controller.estoque;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.estoque.EstoqueContagemCabecalho;
import com.t2ti.siscom.services.estoque.EstoqueContagemCabecalhoService;

@RestController
@RequestMapping("/estoque/contagem")
public class EstoqueContagemCabecalhoController {

	@Autowired
	private EstoqueContagemCabecalhoService service;
	
	@GetMapping
	public List<EstoqueContagemCabecalho> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public EstoqueContagemCabecalho consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public EstoqueContagemCabecalho salvar(@RequestBody EstoqueContagemCabecalho estoqueContagemCabecalho) {
		return service.salvar(estoqueContagemCabecalho);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
