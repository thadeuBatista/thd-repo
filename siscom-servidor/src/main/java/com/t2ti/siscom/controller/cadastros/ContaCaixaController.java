package com.t2ti.siscom.controller.cadastros;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.cadastros.ContaCaixa;
import com.t2ti.siscom.services.cadastros.ContaCaixaService;

@RestController
@RequestMapping("/conta-caixa")
public class ContaCaixaController {

	@Autowired
	private ContaCaixaService service;
	
	@GetMapping
	public List<ContaCaixa> listar() {
		return service.listar();
	}

	@GetMapping("/lista/{nome}")
	public List<ContaCaixa> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public ContaCaixa consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public ContaCaixa salvar(@RequestBody ContaCaixa contaCaixa) {
		return service.salvar(contaCaixa);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
