package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.FinStatusParcela;
import com.t2ti.siscom.services.financeiro.FinStatusParcelaService;

@RestController
@RequestMapping("/financeiro/status-parcela")
public class FinStatusParcelaController {

	@Autowired
	private FinStatusParcelaService service;
	
	@GetMapping
	public List<FinStatusParcela> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<FinStatusParcela> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public FinStatusParcela consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public FinStatusParcela salvar(@RequestBody FinStatusParcela finStatusParcela) {
		return service.salvar(finStatusParcela);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
