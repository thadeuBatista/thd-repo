package com.t2ti.siscom.controller.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.vendas.VendaCabecalho;
import com.t2ti.siscom.services.vendas.VendaCabecalhoService;

@RestController
@RequestMapping("/vendas/cabecalho")
public class VendaCabecalhoController {

	@Autowired
	private VendaCabecalhoService service;
	
	@GetMapping
	public List<VendaCabecalho> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public VendaCabecalho consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public VendaCabecalho salvar(@RequestBody VendaCabecalho vendaCabecalho) {
		return service.salvar(vendaCabecalho);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
