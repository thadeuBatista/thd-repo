package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.Cheque;
import com.t2ti.siscom.services.financeiro.ChequeService;

@RestController
@RequestMapping("/financeiro/cheque")
public class ChequeController {

	@Autowired
	private ChequeService service;
	
	@GetMapping
	public List<Cheque> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{numero}")
	public List<Cheque> listar(@PathVariable Integer numero) {
		return service.listar(numero);
	}
	
	@GetMapping("/{id}")
	public Cheque consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public Cheque salvar(@RequestBody Cheque cheque) {
		return service.salvar(cheque);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
