package com.t2ti.siscom.controller.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.compras.CompraTipoRequisicao;
import com.t2ti.siscom.services.compras.CompraTipoRequisicaoService;

@RestController
@RequestMapping("/compra-tipo-requisicao")
public class CompraTipoRequisicaoController {

	@Autowired
	private CompraTipoRequisicaoService service;
	
	@GetMapping
	public List<CompraTipoRequisicao> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<CompraTipoRequisicao> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public CompraTipoRequisicao consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public CompraTipoRequisicao salvar(@RequestBody CompraTipoRequisicao compraTipoRequisicao) {
		return service.salvar(compraTipoRequisicao);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
