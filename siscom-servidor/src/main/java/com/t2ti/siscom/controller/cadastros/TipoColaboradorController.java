package com.t2ti.siscom.controller.cadastros;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.cadastros.TipoColaborador;
import com.t2ti.siscom.services.cadastros.TipoColaboradorService;

@RestController
@RequestMapping("/tipoColaborador")
public class TipoColaboradorController {

	@Autowired
	private TipoColaboradorService service;
	
	@GetMapping
	public List<TipoColaborador> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<TipoColaborador> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public TipoColaborador consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public TipoColaborador salvar(@RequestBody TipoColaborador tipoColaborador) {
		return service.salvar(tipoColaborador);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
