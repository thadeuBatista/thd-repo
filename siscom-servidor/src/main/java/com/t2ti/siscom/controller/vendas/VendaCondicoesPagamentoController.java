package com.t2ti.siscom.controller.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.vendas.VendaCondicoesPagamento;
import com.t2ti.siscom.services.vendas.VendaCondicoesPagamentoService;

@RestController
@RequestMapping("/vendas/condicoes-pagamento")
public class VendaCondicoesPagamentoController {

	@Autowired
	private VendaCondicoesPagamentoService service;
	
	@GetMapping
	public List<VendaCondicoesPagamento> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<VendaCondicoesPagamento> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public VendaCondicoesPagamento consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public VendaCondicoesPagamento salvar(@RequestBody VendaCondicoesPagamento vendaCondicoesPagamento) {
		return service.salvar(vendaCondicoesPagamento);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
