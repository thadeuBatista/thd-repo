package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.FinParcelaPagar;
import com.t2ti.siscom.services.financeiro.FinParcelaPagamentoService;

@RestController
@RequestMapping("/financeiro/parcela-pagamento")
public class FinParcelaPagamentoController {

	@Autowired
	private FinParcelaPagamentoService service;
	
	@GetMapping
	public List<FinParcelaPagar> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public FinParcelaPagar consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public FinParcelaPagar salvar(@RequestBody FinParcelaPagar finParcelaPagamento) {
		return service.salvar(finParcelaPagamento);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
