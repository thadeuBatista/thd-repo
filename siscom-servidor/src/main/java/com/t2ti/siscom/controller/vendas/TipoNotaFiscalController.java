package com.t2ti.siscom.controller.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.vendas.TipoNotaFiscal;
import com.t2ti.siscom.services.vendas.TipoNotaFiscalService;

@RestController
@RequestMapping("/vendas/tipo-nota-fiscal")
public class TipoNotaFiscalController {

	@Autowired
	private TipoNotaFiscalService service;
	
	@GetMapping
	public List<TipoNotaFiscal> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<TipoNotaFiscal> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public TipoNotaFiscal consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public TipoNotaFiscal salvar(@RequestBody TipoNotaFiscal tipoNotaFiscal) {
		return service.salvar(tipoNotaFiscal);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
