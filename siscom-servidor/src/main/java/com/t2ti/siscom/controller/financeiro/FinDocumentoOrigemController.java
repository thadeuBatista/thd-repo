package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.financeiro.FinDocumentoOrigem;
import com.t2ti.siscom.services.financeiro.FinDocumentoOrigemService;

@RestController
@RequestMapping("/financeiro/documento-origem")
public class FinDocumentoOrigemController {

	@Autowired
	private FinDocumentoOrigemService service;
	
	@GetMapping
	public List<FinDocumentoOrigem> listar() {
		return service.listar();
	}
	
	@GetMapping("/lista/{nome}")
	public List<FinDocumentoOrigem> listar(@PathVariable String nome) {
		return service.listar(nome);
	}
	
	@GetMapping("/{id}")
	public FinDocumentoOrigem consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public FinDocumentoOrigem salvar(@RequestBody FinDocumentoOrigem finDocumentoOrigem) {
		return service.salvar(finDocumentoOrigem);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
