package com.t2ti.siscom.controller.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.compras.CompraRequisicao;
import com.t2ti.siscom.services.compras.CompraRequisicaoService;

@RestController
@RequestMapping("/compra/requisicao")
public class CompraRequisicaoController {

	@Autowired
	private CompraRequisicaoService service;
	
	@GetMapping
	public List<CompraRequisicao> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public CompraRequisicao consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public CompraRequisicao salvar(@RequestBody CompraRequisicao compraRequisicao) {
		return service.salvar(compraRequisicao);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
