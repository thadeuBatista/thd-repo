package com.t2ti.siscom.controller.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.exception.UploadException;
import com.t2ti.siscom.model.financeiro.FinExtratoContaBanco;
import com.t2ti.siscom.services.financeiro.FinExtratoContaBancoService;

@RestController
@RequestMapping("/financeiro/extrato-conta-banco")
public class FinExtratoContaBancoController {

	@Autowired
	private FinExtratoContaBancoService service;
	
	@GetMapping("/lista/{idContaCaixa}/{mesAno}")
	public List<FinExtratoContaBanco> listar(@PathVariable Integer idContaCaixa, @PathVariable String mesAno) {
		return service.listar(idContaCaixa, mesAno);
	}
	
	@GetMapping("/{id}")
	public FinExtratoContaBanco consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
	@PostMapping("/upload/{idContaCaixa}")
	public void uploadExtrato(@RequestParam("extrato") MultipartFile file, @PathVariable Integer idContaCaixa) {
		try {
			service.uploadExtrato(file, idContaCaixa);
		} catch (Exception e) {
			throw new UploadException(e.getMessage());
		}
	}	
}
