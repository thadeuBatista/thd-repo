package com.t2ti.siscom.controller.estoque;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.t2ti.siscom.exception.RecursoNaoEncontradoException;
import com.t2ti.siscom.model.estoque.EstoqueReajusteCabecalho;
import com.t2ti.siscom.services.estoque.EstoqueReajusteCabecalhoService;

@RestController
@RequestMapping("/estoque/reajuste")
public class EstoqueReajusteCabecalhoController {

	@Autowired
	private EstoqueReajusteCabecalhoService service;
	
	@GetMapping
	public List<EstoqueReajusteCabecalho> listar() {
		return service.listar();
	}
	
	@GetMapping("/{id}")
	public EstoqueReajusteCabecalho consultarObjeto(@PathVariable Integer id) {
		try {
			return service.consultarObjeto(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	@PostMapping
	public EstoqueReajusteCabecalho salvar(@RequestBody EstoqueReajusteCabecalho estoqueReajusteCabecalho) {
		return service.salvar(estoqueReajusteCabecalho);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.excluir(id);
	}
	
}
