package com.t2ti.siscom.config;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t2ti.siscom.model.cadastros.Usuario;

public class SiscomAuthentication extends AbstractAuthenticationProcessingFilter {

	protected SiscomAuthentication(String url, AuthenticationManager manager) {
		super(new AntPathRequestMatcher(url));
		System.out.println("Chamou o construtor SiscomAuthentication+++++++++++++++++++++++++++++++");
		setAuthenticationManager(manager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		System.out.println("Chamou o método SiscomAuthentication.attemptAuthentication+++++++++++++++++++++++++++++++");
		Usuario usuario = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);
		
		return getAuthenticationManager().authenticate(
				new UsernamePasswordAuthenticationToken(
						usuario.getLogin(), 
						usuario.getSenha(), 
						new ArrayList<>()
						)
				);
		
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		System.out.println("Chamou o método SiscomAuthentication.successfulAuthentication+++++++++++++++++++++++++++++++");
		
		Usuario usuario = new Usuario();
		usuario.setLogin(authResult.getName());
		usuario.setToken(SiscomToken.geraToken(usuario.getLogin()));
		
		String usuarioJson = new ObjectMapper().writeValueAsString(usuario);
		
		response.getWriter().write(usuarioJson);
	}
	
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Usuário ou senha inválidos");
	}

}
