package com.t2ti.siscom.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SiscomConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("Chamou o método SiscomConfig.configure 01+++++++++++++++++++++++++++++++");
		http.csrf().disable().authorizeRequests()
			.antMatchers("/login").permitAll()
			.antMatchers("/error").permitAll()
			.anyRequest().authenticated()
			.and()
			.addFilterBefore(new SiscomAuthentication("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(new JWTFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		System.out.println("Chamou o método SiscomConfig.configure 02+++++++++++++++++++++++++++++++");
		auth.jdbcAuthentication()
			.dataSource(dataSource)
			.usersByUsernameQuery("SELECT login, senha, 1 as enabled from USUARIO where login=?")
			.authoritiesByUsernameQuery("select login, 'ROLE_USER' from USUARIO where login=?")
			.passwordEncoder(new BCryptPasswordEncoder());
	}
}
