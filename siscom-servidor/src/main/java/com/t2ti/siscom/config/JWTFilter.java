package com.t2ti.siscom.config;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class JWTFilter extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		try {
			
			Authentication authentication = null;
			String token = null;
			if(((HttpServletRequest) request).getHeader("Authorization") != null) {
				token = ((HttpServletRequest) request).getHeader("Authorization").replace("Bearer", "");
			}
			
			if(token != null) {
				String login = SiscomToken.verificaToken(token);
				
				authentication = new UsernamePasswordAuthenticationToken(login, null, new ArrayList<>());
			}
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
			chain.doFilter(request, response);
		} catch (Exception e) {
			((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
		}

	}

}
