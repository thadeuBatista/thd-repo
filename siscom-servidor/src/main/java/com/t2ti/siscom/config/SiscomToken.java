package com.t2ti.siscom.config;

import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class SiscomToken {
	
	public static final String CHAVE = "T2Ti - SisCom - Chave Token";
	
	public static String geraToken(String subject) {
		System.out.println("Chamou o método geraToken+++++++++++++++++++++++++++++++");
		String token = Jwts.builder()
							.setSubject(subject)
							.setExpiration(new Date(System.currentTimeMillis() + (1 * 60000)))
							.signWith(SignatureAlgorithm.HS512, CHAVE)
							.compact();
		return token;
	}
	
	public static String verificaToken(String token) throws Exception {
		System.out.println("Chamou o método verificaToken+++++++++++++++++++++++++++++++");
		if(token != null) {
			try {
				return Jwts.parser()
					.setSigningKey(CHAVE)
					.parseClaimsJws(token)
					.getBody()
					.getSubject();
			}catch (Exception e) {
				throw new Exception("Token inválido ou expirado.");
			}
			
		}
		throw new Exception("Necessário informar o token.");
	}
	

}
