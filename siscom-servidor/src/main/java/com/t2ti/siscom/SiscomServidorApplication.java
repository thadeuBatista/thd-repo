package com.t2ti.siscom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SiscomServidorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiscomServidorApplication.class, args);
		//geraSenha();
	}
	
	private static void geraSenha() {
		System.out.println(new BCryptPasswordEncoder().encode("123"));
	}

}
