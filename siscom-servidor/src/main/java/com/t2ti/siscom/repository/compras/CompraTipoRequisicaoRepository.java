package com.t2ti.siscom.repository.compras;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.compras.CompraTipoRequisicao;

public interface CompraTipoRequisicaoRepository extends JpaRepository<CompraTipoRequisicao, Integer> {

	List<CompraTipoRequisicao> findFirst10ByNomeContaining(String nome);
	
}