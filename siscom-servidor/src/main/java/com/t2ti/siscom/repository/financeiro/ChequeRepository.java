package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.Cheque;

public interface ChequeRepository extends JpaRepository<Cheque, Integer> {

	List<Cheque> findByNumero(Integer numero);
}