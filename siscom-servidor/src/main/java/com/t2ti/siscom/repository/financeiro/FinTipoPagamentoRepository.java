package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.FinTipoPagamento;

public interface FinTipoPagamentoRepository extends JpaRepository<FinTipoPagamento, Integer> {

	List<FinTipoPagamento> findFirst10ByDescricaoContaining(String descricao);
}