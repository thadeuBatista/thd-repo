package com.t2ti.siscom.repository.vendas;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.vendas.VendaComissao;

public interface VendaComissaoRepository extends JpaRepository<VendaComissao, Integer> {

}