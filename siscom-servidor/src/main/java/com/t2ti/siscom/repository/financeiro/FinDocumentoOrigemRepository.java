package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.FinDocumentoOrigem;

public interface FinDocumentoOrigemRepository extends JpaRepository<FinDocumentoOrigem, Integer> {

	List<FinDocumentoOrigem> findFirst10BySiglaDocumentoContaining(String siglaDocumento);
}