package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.cadastros.ContaCaixa;
import com.t2ti.siscom.model.financeiro.FinExtratoContaBanco;

public interface FinExtratoContaBancoRepository extends JpaRepository<FinExtratoContaBanco, Integer> {

	List<FinExtratoContaBanco> findByContaCaixaAndMesAno(ContaCaixa contaCaixa, String mesAno);
}