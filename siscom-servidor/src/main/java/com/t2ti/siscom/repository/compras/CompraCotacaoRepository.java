package com.t2ti.siscom.repository.compras;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.compras.CompraCotacao;

public interface CompraCotacaoRepository extends JpaRepository<CompraCotacao, Integer> {

	
}