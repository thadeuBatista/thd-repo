package com.t2ti.siscom.repository.vendas;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.vendas.VendaCabecalho;

public interface VendaCabecalhoRepository extends JpaRepository<VendaCabecalho, Integer> {

}