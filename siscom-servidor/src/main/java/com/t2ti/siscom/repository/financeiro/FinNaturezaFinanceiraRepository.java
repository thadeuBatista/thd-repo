package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.FinNaturezaFinanceira;

public interface FinNaturezaFinanceiraRepository extends JpaRepository<FinNaturezaFinanceira, Integer> {

	List<FinNaturezaFinanceira> findFirst10ByCodigoContaining(String codigo);
}