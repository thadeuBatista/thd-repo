package com.t2ti.siscom.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.vendas.VendaCondicoesPagamento;

public interface VendaCondicoesPagamentoRepository extends JpaRepository<VendaCondicoesPagamento, Integer> {

	List<VendaCondicoesPagamento> findFirst10ByNomeContaining(String nome);
}