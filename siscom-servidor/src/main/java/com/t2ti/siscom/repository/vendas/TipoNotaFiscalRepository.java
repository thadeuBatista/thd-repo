package com.t2ti.siscom.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.vendas.TipoNotaFiscal;

public interface TipoNotaFiscalRepository extends JpaRepository<TipoNotaFiscal, Integer> {

	List<TipoNotaFiscal> findFirst10ByNomeContaining(String nome);
}