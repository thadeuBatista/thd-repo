package com.t2ti.siscom.repository.financeiro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.FinParcelaPagar;

public interface FinParcelaPagamentoRepository extends JpaRepository<FinParcelaPagar, Integer> {

}