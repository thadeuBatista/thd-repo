package com.t2ti.siscom.repository.estoque;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.estoque.EstoqueReajusteCabecalho;

public interface EstoqueReajusteCabecalhoRepository extends JpaRepository<EstoqueReajusteCabecalho, Integer> {

	
}