package com.t2ti.siscom.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.vendas.VendaOrcamentoCabecalho;

public interface VendaOrcamentoCabecalhoRepository extends JpaRepository<VendaOrcamentoCabecalho, Integer> {

	List<VendaOrcamentoCabecalho> findFirst10ByCodigoContaining(String nome);
}