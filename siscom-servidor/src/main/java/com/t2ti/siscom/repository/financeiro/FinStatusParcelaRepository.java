package com.t2ti.siscom.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.t2ti.siscom.model.financeiro.FinStatusParcela;

public interface FinStatusParcelaRepository extends JpaRepository<FinStatusParcela, Integer> {

	List<FinStatusParcela> findFirst10ByDescricaoContaining(String descricao);
}