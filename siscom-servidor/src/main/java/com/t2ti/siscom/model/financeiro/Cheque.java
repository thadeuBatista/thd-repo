package com.t2ti.siscom.model.financeiro;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the cheque database table.
 * 
 */
@Entity
@NamedQuery(name="Cheque.findAll", query="SELECT c FROM Cheque c")
public class Cheque implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_STATUS")
	private Date dataStatus;

	private Integer numero;

	@Column(name="STATUS_CHEQUE")
	private String statusCheque;

	//bi-directional many-to-one association to TalonarioCheque
	@ManyToOne
	@JoinColumn(name="ID_TALONARIO_CHEQUE")
	private TalonarioCheque talonarioCheque;

	public Cheque() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataStatus() {
		return this.dataStatus;
	}

	public void setDataStatus(Date dataStatus) {
		this.dataStatus = dataStatus;
	}

	public Integer getNumero() {
		return this.numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getStatusCheque() {
		return this.statusCheque;
	}

	public void setStatusCheque(String statusCheque) {
		this.statusCheque = statusCheque;
	}

	public TalonarioCheque getTalonarioCheque() {
		return this.talonarioCheque;
	}

	public void setTalonarioCheque(TalonarioCheque talonarioCheque) {
		this.talonarioCheque = talonarioCheque;
	}

}