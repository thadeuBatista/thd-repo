package com.t2ti.siscom.model.financeiro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.t2ti.siscom.model.cadastros.ContaCaixa;


/**
 * The persistent class for the talonario_cheque database table.
 * 
 */
@Entity
@Table(name="talonario_cheque")
@NamedQuery(name="TalonarioCheque.findAll", query="SELECT t FROM TalonarioCheque t")
public class TalonarioCheque implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer numero;

	@Column(name="STATUS_TALAO")
	private String statusTalao;

	private String talao;

	//bi-directional many-to-one association to ContaCaixa
	@ManyToOne
	@JoinColumn(name="ID_CONTA_CAIXA")
	private ContaCaixa contaCaixa;

	public TalonarioCheque() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumero() {
		return this.numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getStatusTalao() {
		return this.statusTalao;
	}

	public void setStatusTalao(String statusTalao) {
		this.statusTalao = statusTalao;
	}

	public String getTalao() {
		return this.talao;
	}

	public void setTalao(String talao) {
		this.talao = talao;
	}

	public ContaCaixa getContaCaixa() {
		return this.contaCaixa;
	}

	public void setContaCaixa(ContaCaixa contaCaixa) {
		this.contaCaixa = contaCaixa;
	}

}