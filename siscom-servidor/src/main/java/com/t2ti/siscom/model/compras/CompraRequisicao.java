package com.t2ti.siscom.model.compras;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.t2ti.siscom.model.cadastros.Colaborador;


/**
 * The persistent class for the compra_requisicao database table.
 * 
 */
@Entity
@Table(name="compra_requisicao")
@NamedQuery(name="CompraRequisicao.findAll", query="SELECT c FROM CompraRequisicao c")
public class CompraRequisicao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_REQUISICAO")
	private Date dataRequisicao;

	//bi-directional many-to-one association to CompraTipoRequisicao
	@ManyToOne
	@JoinColumn(name="ID_COMPRA_TIPO_REQUISICAO")
	private CompraTipoRequisicao compraTipoRequisicao;

	//bi-directional many-to-one association to Colaborador
	@ManyToOne
	@JoinColumn(name="ID_COLABORADOR")
	private Colaborador colaborador;

	@OneToMany(mappedBy = "compraRequisicao", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<CompraRequisicaoDetalhe> listaCompraRequisicaoDetalhe;
	
	public CompraRequisicao() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataRequisicao() {
		return this.dataRequisicao;
	}

	public void setDataRequisicao(Date dataRequisicao) {
		this.dataRequisicao = dataRequisicao;
	}

	public CompraTipoRequisicao getCompraTipoRequisicao() {
		return this.compraTipoRequisicao;
	}

	public void setCompraTipoRequisicao(CompraTipoRequisicao compraTipoRequisicao) {
		this.compraTipoRequisicao = compraTipoRequisicao;
	}

	public Colaborador getColaborador() {
		return this.colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Set<CompraRequisicaoDetalhe> getListaCompraRequisicaoDetalhe() {
		return listaCompraRequisicaoDetalhe;
	}

	public void setListaCompraRequisicaoDetalhe(Set<CompraRequisicaoDetalhe> listaCompraRequisicaoDetalhe) {
		this.listaCompraRequisicaoDetalhe = listaCompraRequisicaoDetalhe;
	}


}