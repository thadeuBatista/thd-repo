package com.t2ti.siscom.model.compras;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the compra_cotacao_pedido_detalhe database table.
 * 
 */
@Entity
@Table(name="compra_cotacao_pedido_detalhe")
@NamedQuery(name="CompraCotacaoPedidoDetalhe.findAll", query="SELECT c FROM CompraCotacaoPedidoDetalhe c")
public class CompraCotacaoPedidoDetalhe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="QUANTIDADE_PEDIDA")
	private BigDecimal quantidadePedida;

	//bi-directional many-to-one association to CompraCotacaoDetalhe
	@ManyToOne
	@JoinColumn(name="ID_COMPRA_COTACAO_DETALHE")
	private CompraCotacaoDetalhe compraCotacaoDetalhe;

	//bi-directional many-to-one association to CompraPedido
	@ManyToOne
	@JoinColumn(name="ID_COMPRA_PEDIDO")
	private CompraPedido compraPedido;

	public CompraCotacaoPedidoDetalhe() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getQuantidadePedida() {
		return this.quantidadePedida;
	}

	public void setQuantidadePedida(BigDecimal quantidadePedida) {
		this.quantidadePedida = quantidadePedida;
	}

	public CompraCotacaoDetalhe getCompraCotacaoDetalhe() {
		return this.compraCotacaoDetalhe;
	}

	public void setCompraCotacaoDetalhe(CompraCotacaoDetalhe compraCotacaoDetalhe) {
		this.compraCotacaoDetalhe = compraCotacaoDetalhe;
	}

	public CompraPedido getCompraPedido() {
		return this.compraPedido;
	}

	public void setCompraPedido(CompraPedido compraPedido) {
		this.compraPedido = compraPedido;
	}

}