package com.t2ti.siscom.model.estoque;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the estoque_contagem_cabecalho database table.
 * 
 */
@Entity
@Table(name = "estoque_contagem_cabecalho")
@NamedQuery(name = "EstoqueContagemCabecalho.findAll", query = "SELECT e FROM EstoqueContagemCabecalho e")
public class EstoqueContagemCabecalho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_CONTAGEM")
	private Date dataContagem;

	@Column(name = "ESTOQUE_ATUALIZADO")
	private String estoqueAtualizado;

	// bi-directional many-to-one association to EstoqueContagemDetalhe
	@OneToMany(mappedBy = "estoqueContagemCabecalho", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EstoqueContagemDetalhe> listaEstoqueContagemDetalhe;

	public EstoqueContagemCabecalho() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataContagem() {
		return this.dataContagem;
	}

	public void setDataContagem(Date dataContagem) {
		this.dataContagem = dataContagem;
	}

	public String getEstoqueAtualizado() {
		return this.estoqueAtualizado;
	}

	public void setEstoqueAtualizado(String estoqueAtualizado) {
		this.estoqueAtualizado = estoqueAtualizado;
	}

	public Set<EstoqueContagemDetalhe> getListaEstoqueContagemDetalhe() {
		return listaEstoqueContagemDetalhe;
	}

	public void setListaEstoqueContagemDetalhe(Set<EstoqueContagemDetalhe> listaEstoqueContagemDetalhe) {
		this.listaEstoqueContagemDetalhe = listaEstoqueContagemDetalhe;
	}

}