package com.t2ti.siscom.model.vendas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.t2ti.siscom.model.cadastros.Vendedor;


/**
 * The persistent class for the venda_comissao database table.
 * 
 */
@Entity
@Table(name="venda_comissao")
@NamedQuery(name="VendaComissao.findAll", query="SELECT v FROM VendaComissao v")
public class VendaComissao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_LANCAMENTO")
	private Date dataLancamento;

	private String situacao;

	@Column(name="TIPO_CONTABIL")
	private String tipoContabil;

	@Column(name="VALOR_COMISSAO")
	private BigDecimal valorComissao;

	@Column(name="VALOR_VENDA")
	private BigDecimal valorVenda;

	//bi-directional many-to-one association to VendaCabecalho
	@ManyToOne
	@JoinColumn(name="ID_VENDA_CABECALHO")
	private VendaCabecalho vendaCabecalho;

	//bi-directional many-to-one association to Vendedor
	@ManyToOne
	@JoinColumn(name="ID_VENDEDOR")
	private Vendedor vendedor;

	public VendaComissao() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDataLancamento() {
		return this.dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public String getSituacao() {
		return this.situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTipoContabil() {
		return this.tipoContabil;
	}

	public void setTipoContabil(String tipoContabil) {
		this.tipoContabil = tipoContabil;
	}

	public BigDecimal getValorComissao() {
		return this.valorComissao;
	}

	public void setValorComissao(BigDecimal valorComissao) {
		this.valorComissao = valorComissao;
	}

	public BigDecimal getValorVenda() {
		return this.valorVenda;
	}

	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}

	public VendaCabecalho getVendaCabecalho() {
		return this.vendaCabecalho;
	}

	public void setVendaCabecalho(VendaCabecalho vendaCabecalho) {
		this.vendaCabecalho = vendaCabecalho;
	}

	public Vendedor getVendedor() {
		return this.vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

}