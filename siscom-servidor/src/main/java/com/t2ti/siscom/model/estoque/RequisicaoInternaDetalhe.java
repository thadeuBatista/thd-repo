package com.t2ti.siscom.model.estoque;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.t2ti.siscom.model.cadastros.Produto;


/**
 * The persistent class for the requisicao_interna_detalhe database table.
 * 
 */
@Entity
@Table(name="requisicao_interna_detalhe")
@NamedQuery(name="RequisicaoInternaDetalhe.findAll", query="SELECT r FROM RequisicaoInternaDetalhe r")
public class RequisicaoInternaDetalhe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private BigDecimal quantidade;

	//bi-directional many-to-one association to RequisicaoInternaCabecalho
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="ID_REQ_INTERNA_CABECALHO")
	private RequisicaoInternaCabecalho requisicaoInternaCabecalho;

	//bi-directional many-to-one association to Produto
	@ManyToOne
	@JoinColumn(name="ID_PRODUTO")
	private Produto produto;

	public RequisicaoInternaDetalhe() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public RequisicaoInternaCabecalho getRequisicaoInternaCabecalho() {
		return this.requisicaoInternaCabecalho;
	}

	public void setRequisicaoInternaCabecalho(RequisicaoInternaCabecalho requisicaoInternaCabecalho) {
		this.requisicaoInternaCabecalho = requisicaoInternaCabecalho;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}