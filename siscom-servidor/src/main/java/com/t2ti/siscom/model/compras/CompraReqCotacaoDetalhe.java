package com.t2ti.siscom.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the compra_req_cotacao_detalhe database table.
 * 
 */
@Entity
@Table(name="compra_req_cotacao_detalhe")
@NamedQuery(name="CompraReqCotacaoDetalhe.findAll", query="SELECT c FROM CompraReqCotacaoDetalhe c")
public class CompraReqCotacaoDetalhe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name="QUANTIDADE_COTADA")
	private BigDecimal quantidadeCotada;

	//bi-directional many-to-one association to CompraRequisicaoDetalhe
	@ManyToOne
	@JoinColumn(name="ID_COMPRA_REQUISICAO_DETALHE")
	private CompraRequisicaoDetalhe compraRequisicaoDetalhe;

	//bi-directional many-to-one association to CompraCotacao
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="ID_COMPRA_COTACAO")
	private CompraCotacao compraCotacao;

	public CompraReqCotacaoDetalhe() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getQuantidadeCotada() {
		return this.quantidadeCotada;
	}

	public void setQuantidadeCotada(BigDecimal quantidadeCotada) {
		this.quantidadeCotada = quantidadeCotada;
	}

	public CompraRequisicaoDetalhe getCompraRequisicaoDetalhe() {
		return this.compraRequisicaoDetalhe;
	}

	public void setCompraRequisicaoDetalhe(CompraRequisicaoDetalhe compraRequisicaoDetalhe) {
		this.compraRequisicaoDetalhe = compraRequisicaoDetalhe;
	}

	public CompraCotacao getCompraCotacao() {
		return this.compraCotacao;
	}

	public void setCompraCotacao(CompraCotacao compraCotacao) {
		this.compraCotacao = compraCotacao;
	}

}