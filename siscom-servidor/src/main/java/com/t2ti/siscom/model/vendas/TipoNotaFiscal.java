package com.t2ti.siscom.model.vendas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the tipo_nota_fiscal database table.
 * 
 */
@Entity
@Table(name="tipo_nota_fiscal")
@NamedQuery(name="TipoNotaFiscal.findAll", query="SELECT t FROM TipoNotaFiscal t")
public class TipoNotaFiscal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Lob
	private String descricao;

	private String modelo;

	private String nome;

	@Column(name="NUMERO_ITENS")
	private Integer numeroItens;

	private String serie;

	@Lob
	private String template;

	@Column(name="ULTIMO_IMPRESSO")
	private Integer ultimoImpresso;

	public TipoNotaFiscal() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getModelo() {
		return this.modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNumeroItens() {
		return this.numeroItens;
	}

	public void setNumeroItens(Integer numeroItens) {
		this.numeroItens = numeroItens;
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getTemplate() {
		return this.template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Integer getUltimoImpresso() {
		return this.ultimoImpresso;
	}

	public void setUltimoImpresso(Integer ultimoImpresso) {
		this.ultimoImpresso = ultimoImpresso;
	}

}