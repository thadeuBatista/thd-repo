package com.t2ti.siscom.model.vendas;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;


/**
 * The persistent class for the venda_condicoes_parcelas database table.
 * 
 */
@Entity
@Table(name="venda_condicoes_parcelas")
@NamedQuery(name="VendaCondicoesParcela.findAll", query="SELECT v FROM VendaCondicoesParcela v")
public class VendaCondicoesParcela implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer dias;

	private Integer parcela;

	private BigDecimal taxa;

	//bi-directional many-to-one association to VendaCondicoesPagamento
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="ID_VENDA_CONDICOES_PAGAMENTO")
	private VendaCondicoesPagamento vendaCondicoesPagamento;

	public VendaCondicoesParcela() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDias() {
		return this.dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public Integer getParcela() {
		return this.parcela;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public BigDecimal getTaxa() {
		return this.taxa;
	}

	public void setTaxa(BigDecimal taxa) {
		this.taxa = taxa;
	}

	public VendaCondicoesPagamento getVendaCondicoesPagamento() {
		return this.vendaCondicoesPagamento;
	}

	public void setVendaCondicoesPagamento(VendaCondicoesPagamento vendaCondicoesPagamento) {
		this.vendaCondicoesPagamento = vendaCondicoesPagamento;
	}

}