package com.t2ti.siscom.model.estoque;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.t2ti.siscom.model.cadastros.Produto;


/**
 * The persistent class for the estoque_contagem_detalhe database table.
 * 
 */
@Entity
@Table(name="estoque_contagem_detalhe")
@NamedQuery(name="EstoqueContagemDetalhe.findAll", query="SELECT e FROM EstoqueContagemDetalhe e")
public class EstoqueContagemDetalhe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private BigDecimal acuracidade;

	private BigDecimal divergencia;

	@Column(name="QUANTIDADE_CONTADA")
	private BigDecimal quantidadeContada;

	@Column(name="QUANTIDADE_SISTEMA")
	private BigDecimal quantidadeSistema;

	//bi-directional many-to-one association to EstoqueContagemCabecalho
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="ID_ESTOQUE_CONTAGEM_CABECALHO")
	private EstoqueContagemCabecalho estoqueContagemCabecalho;

	//bi-directional many-to-one association to Produto
	@ManyToOne
	@JoinColumn(name="ID_PRODUTO")
	private Produto produto;

	public EstoqueContagemDetalhe() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAcuracidade() {
		return this.acuracidade;
	}

	public void setAcuracidade(BigDecimal acuracidade) {
		this.acuracidade = acuracidade;
	}

	public BigDecimal getDivergencia() {
		return this.divergencia;
	}

	public void setDivergencia(BigDecimal divergencia) {
		this.divergencia = divergencia;
	}

	public BigDecimal getQuantidadeContada() {
		return this.quantidadeContada;
	}

	public void setQuantidadeContada(BigDecimal quantidadeContada) {
		this.quantidadeContada = quantidadeContada;
	}

	public BigDecimal getQuantidadeSistema() {
		return this.quantidadeSistema;
	}

	public void setQuantidadeSistema(BigDecimal quantidadeSistema) {
		this.quantidadeSistema = quantidadeSistema;
	}

	public EstoqueContagemCabecalho getEstoqueContagemCabecalho() {
		return this.estoqueContagemCabecalho;
	}

	public void setEstoqueContagemCabecalho(EstoqueContagemCabecalho estoqueContagemCabecalho) {
		this.estoqueContagemCabecalho = estoqueContagemCabecalho;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}